﻿angular.module('umbraco').controller('Msl.Grid.ButtonPropertiesController', function ($scope, $rootScope, $timeout, userService, dialogService) {

    SetButtonProperties("FFFFFF", "000000");

    // Set hasLink
    $scope.hasLink = false;
    $scope.linkType = "Add link";

        $scope.elementText = "";
        $scope.elementLink = "";
        $scope.target = "";

    if ($scope.model.editModel != null) {
        var buttonModel = $scope.model.editModel;
        if (buttonModel.buttonLink != null) {
            $scope.hasLink = true;
            $scope.linkType = "Edit Link";
        }

        $scope.buttonProperties = {
            buttonName: buttonModel.buttonName,
            buttonTextColor: buttonModel.buttonTextColor,
            buttonColor: buttonModel.buttonColor,
            buttonLink: buttonModel.buttonLink
        }

        $scope.elementText = buttonModel.buttonName;
        $scope.elementLink = buttonModel.buttonLink;
        if (buttonModel.buttonTarget != null) {
            $scope.target = buttonModel.buttonTarget;
        }


        $scope.buttonColor = buttonModel.buttonColor;
        $scope.textColor = buttonModel.buttonTextColor;

        console.log($scope.buttonColor + " " + $scope.textColor);
        SetButtonProperties($scope.buttonColor, $scope.textColor );
    }

    // Link Picker

    $scope.ChooseLink = function () {
        dialogService.linkPicker({
            currentTarget: {
                name: $scope.elementText,
                url: $scope.elementLink,
                target: $scope.target
            },
            callback: function (data) {
                $scope.buttonProperties = {
                    buttonName: data.name,
                    buttonLink: data.url,
                    buttonTarget: data.target,
                    buttonBackground: $scope.buttonBackground,
                    buttonTextColor: $scope.myProperty
                };

                $scope.model.value = $scope.buttonProperties;
                console.log($scope.model.value);

                $scope.buttonText = data.name;
                $scope.buttonLink = data.url;
                $scope.target = data.target;
                $scope.hasLink = true;
            }
        });
    };

    function SetButtonProperties(buttonColor,textColor) {
        // Property Editors
        $scope.myProperty = {
            view: '/App_Plugins/Grid/editors/colorpicker.html',
            config: {
                items: [
                    { value: "000000", label: "Black" },
                    { value: "FFFFFF", label: "White" },
                    { value: "09345E", label: "Dark Blue" }
                ]
            },
            value: textColor
        };

        $scope.buttonBackground = {
            view: '/App_Plugins/Grid/editors/colorpicker.html',
            //editModel: $scope.model.value,
            config: {
                items: [
                    { value: "2E74B5", label: "Light Blue" },
                    { value: "E0DD76", label: "Yellow" },
                    { value: "09345E", label: "Dark Blue" }
                ]

            },
            callback: function (data) {
                alert("changed background");
            },
            value: buttonColor
        };
    };


    $scope.$watch('buttonBackground.value', function (newValue, oldValue) {
        if ($scope.hasLink) {
            $scope.buttonProperties.buttonBackground = newValue.value;
            console.log($scope.buttonProperties);
            AddToButtonProperties();
        }
        

    });

    $scope.$watch('myProperty.value', function (newValue, oldValue) {
        if ($scope.hasLink) {
            $scope.buttonProperties.buttonTextColor = newValue.value;
            console.log($scope.buttonProperties);
            AddToButtonProperties();
        }
        
    });

    function AddToButtonProperties() {
        $scope.model.value = $scope.buttonProperties;
    }

});