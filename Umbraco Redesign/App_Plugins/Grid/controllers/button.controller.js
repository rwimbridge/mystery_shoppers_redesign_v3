﻿angular.module('umbraco').controller('Msl.Grid.ButtonController', function ($scope, $rootScope, $timeout, userService, dialogService) {


    $scope.buttonProperties = '';
    $scope.buttonOptions = null;

    $scope.openOverlay = function () {
        if ($scope.control.value != null) {
            $scope.title = "Edit Button";
        }
        else {
            $scope.title = "Add Button";
        }

        $scope.overlay = {
            view: "/App_Plugins/Grid/buttonproperties.html",
            title: $scope.title,
            editModel: $scope.control.value,
            show: true,
            submit: function (model) {
                console.log("overlay model");
                console.log(model);

                var buttonName;
                var buttonText;
                var buttonColor;
                var buttonLink;
                var buttonTarget;
                var buttonClass;


                if (model.value != null) {
                    console.log(model.value);
                    var buttonBackground;
                    var buttonTextColor;

                    if (model.value.buttonBackground.value == null) {
                        buttonBackground = model.value.buttonBackground;
                    }
                    else {
                        buttonBackground = model.value.buttonBackground.value.value;
                    }

                    if (model.value.buttonTextColor.value == null) {
                        buttonTextColor = model.value.buttonTextColor;
                    }
                    else {
                        buttonTextColor = model.value.buttonTextColor.value.value;
                    }

                    // Set the button Options
                    buttonName = model.value.buttonName,
                    buttonText = buttonTextColor,
                    buttonColor = buttonBackground,
                    buttonLink = model.value.buttonLink,
                    buttonTarget = model.value.buttonTarget,
                    buttonClass = "text-align:center; line-height:40px"

                }
                else {

                    buttonName = $scope.control.value.buttonName,
                    buttonText = $scope.control.value.buttonTextColor,
                    buttonColor = $scope.control.value.buttonColor,
                    buttonLink = $scope.control.value.buttonLink,
                    buttonTarget = $scope.control.value.buttonTarget,
                    buttonClass = "text-align:center; line-height:40px"
                }

                $scope.buttonOptions = {
                    buttonName: buttonName,
                    buttonTextColor: buttonText,
                    buttonColor: buttonColor,
                    buttonLink: buttonLink,
                    buttonTarget: buttonTarget,
                    buttonClass: buttonClass

                }
                console.log($scope.buttonOptions);
                $scope.control.value = $scope.buttonOptions;

                $scope.overlay.show = false;
                $scope.overlay = null;

            },
            close: function (oldModel) {
                $scope.overlay.show = false;
                $scope.overlay = null;
            }
        };
    };

});



