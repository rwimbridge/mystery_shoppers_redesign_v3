﻿// add the resource to umbraco.resources module
angular.module('umbraco.resources').factory('mslIconResource',
    function ($q, $http) {
        // the factory object returned
        return {
            // call the Api Controller to get the data
            getAll: function () {

                var result = $http.get("backoffice/mslIcons/MSLIconsApi/GetAllIcons");
                console.log(result);

                return result;
            },
            getIconList: function () {
                var result = $http.post("backoffice/mslIcons/MSLIconsApi/AddAllIcons");

                return result;
            },
            getFilteredList: function (searchQuery) {
                var result = $http.get("backoffice/mslIcons/MSLIconsApi/GetFilteredIcons/", { params: {searchQuery: searchQuery}});

                return result;
            }

        }

    }


);