﻿angular.module("umbraco").controller("mslIcons.mslIconsController", function ($scope) {

    var icon = "";

    $scope.icons = [
        { id: "0", value: "-1", text: "Please Select"},
        { id: "1", value: "fab fa-facebook-square", text: "Facebook" },
        { id: "2", value: "fab fa-linkedin", text: "Linked In" },
        { id: "3", value: "fab fa-youtube", text: "You Tube" },
        { id: "4", value: "fab fa-instagram", text: "Instagram" }
    ];

    console.log($scope.icons);

    $scope.seticon = function () {
        $scope.selectedIcon = $scope.model.value;
    };

    if ($scope.model.value !== "") {
        $scope.selectedIcon = $scope.model.value;
    }
    else {
        $scope.selectedIcon = "-1";
        $scope.model.value = $scope.selectedIcon;
    }

});