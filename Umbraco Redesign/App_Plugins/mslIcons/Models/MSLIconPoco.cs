﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Umbraco_Redesign.App_Plugins.mslIcons.Models.Data.Poco
{
    [TableName("MSLIcons")]
    [PrimaryKey("id", autoIncrement = true)]
    public class MSLIconPoco
    {
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int id { get; set; }

        [Column("name")]
        public string name { get; set; }

        [Column("className")]
        public string className { get; set; }

    }
}