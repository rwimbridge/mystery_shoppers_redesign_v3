﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using Umbraco.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using Umbraco.Web.Editors;
using Umbraco_Redesign.Models;
using System.Text.RegularExpressions;
using Umbraco_Redesign.App_Plugins.mslIcons.Models.Data.Poco;

namespace Umbraco_Redesign.Controllers
{
    [PluginController("mslIcons")]
    public class MSLIconsApiController : UmbracoAuthorizedJsonController
    {
        public IEnumerable<MSLIcon> GetAllIcons()
        {
            // get the database
            var db = UmbracoContext.Application.DatabaseContext.Database;

            // build a query to select everything in the MSLIcons table
            var query = new Sql().Select("name", "className").From("MSLIcons").OrderBy("className");

            // Fetch data from DB with the query and map to MSLicon object
            return db.Fetch<MSLIcon>(query);
        }

        public IEnumerable<MSLIcon> AddAllIcons()
        {
            string iconFile = File.ReadAllText(@"Z:\font-awesome.css");

            //string[] lines = File.ReadAllLines(@"Z:\all.css.txt");
            //Regex rx = new Regex(@"^[.]([fa]||[fab]||[fas])[-]([a-z0-9-])([a-z0-9-])+([^:])", RegexOptions.Multiline);
            Regex rx = new Regex(@"^[.][fa]+[-]([a-z-0-9]+)", RegexOptions.Multiline);
            MatchCollection matches = rx.Matches(iconFile);

            // get the database
            var db = UmbracoContext.Application.DatabaseContext.Database;



            foreach (Match match in matches)
            {

                
                // build a query to select everything in the MSLIcons table
                var iconClass = match.ToString().Replace(".","");
                //iconClass = "fa " + iconClass;
                var firstHypen = iconClass.IndexOf("-", 0);
                var iconClassLength = iconClass.Length;
                var className = match.ToString();
                //className = iconClass.Replace("fa-", "");
                //Regex pattern = new Regex(@"(\w[^.fa-])[a-z-]+");
                //var patternMatch = pattern.Match(match.ToString());


                //className = patternMatch.ToString().Replace(":","");

                





                //var query = new Sql().Select("*").From("MSLIcons").Where<>("class = '"+ iconClass + "'");
                var query = new Sql().Select("*").From("MSLIcons").Where("className = @0", "fa " + iconClass);
                //var query = "SELECT name,className FROM MSLIcons Where className = '" + iconClass + "'";

                var result = db.Fetch<MSLIconPoco>(query).FirstOrDefault();

                if( result == null)
                {

                    if(className != "")
                    {
                        if (firstHypen != -1 && iconClassLength > 0)
                        {
                            //className = iconClass.Substring(firstHypen + 1, (iconClassLength - firstHypen) - 1).Replace("-", " ");
                            className = iconClass.Replace("-", " ").Replace("fa ", "");
                        }


                        iconClass = "fa " + iconClass;
                        var iconToAdd = new MSLIconPoco
                        {
                            name = className,
                            className = iconClass
                        };

                        db.Insert(iconToAdd);
                    }
                    

                    //var iconToAdd = new MSLIcon();
                    //iconToAdd.name = className;
                    //iconToAdd.className = iconClass;

                    //db.Insert(iconToAdd);

                    
                    //result.className = iconClass;

                    

                    //db.Update(result);

                }

                

            }

            var returnQuery = new Sql().Select("name", "className").From("MSLIcons").OrderBy("className");

            return db.Fetch<MSLIcon>(returnQuery);

            //foreach (Match match in matches)
            //{
            //    var icon = match.
            //}

        }

        
        public IEnumerable<MSLIcon> GetFilteredIcons(string searchQuery)
        {

            //var searchQuery = "facebook";
            // get the database
            var db = UmbracoContext.Application.DatabaseContext.Database;

            // build a query to select everything in the MSLIcons table
            var query = new Sql().Select("name", "className").From("MSLIcons").Where("name like" + "'%" + searchQuery + "%'").OrderBy("className");

            // Fetch data from DB with the query and map to MSLicon object
            return db.Fetch<MSLIcon>(query);

        }

    }
}