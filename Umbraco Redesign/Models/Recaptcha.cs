﻿using System;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;

namespace Umbraco_Redesign.Models
{
    public static class Recaptcha
    {

        public static bool ValidateCaptcha(string response)
        {
            bool Valid = false;

            // Request to google Server


            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("https://www.google.com/recaptcha/api/siteverify?secret=6LdYjzsUAAAAAEt7YI8VWZ6N_lLS0kH9Wt2yVSnm&response=" + response);

            try
            {
                using (WebResponse wResponse = req.GetResponse())
                {
                    using (StreamReader readStream = new StreamReader(wResponse.GetResponseStream()))
                    {
                        string jsonResponse = readStream.ReadToEnd();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        CaptchaData jholder = js.Deserialize<CaptchaData>(jsonResponse);


                        Valid = Convert.ToBoolean(jholder.success);
                    }
                }
                return Valid;
            }
            catch (WebException ex)
            {
                throw ex;
            }
        }

        public class CaptchaData
        {
            public string success { get; set; }
        }
    }
}