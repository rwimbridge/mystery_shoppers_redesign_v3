﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco_Redesign.Models
{
    public class OfficeProperty
    {
        public string officeLabel { get; set; }
        public string officeContent { get; set; }
    }
}