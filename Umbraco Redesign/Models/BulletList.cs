﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco_Redesign.Models
{
    public class BulletList
    {
        public int Id { get; set; }
        public string Contenturl { get; set; }
        public object IconName { get; set; }
        public string ListItemDescription { get; set; }
        public string Title { get; set; }
    }
}