﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco_Redesign.Models
{
    public class CarouselModel
    {
        public int carouselId { get; set; }
        public bool FullWidth { get; set; }
        public List<SectionWithSlidesModel> slides { get; set; }
        public object TextCarousel { get; set; }
        public string CarouselSpeed { get; set; }
    }
}