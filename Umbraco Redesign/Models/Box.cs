﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;

namespace Umbraco_Redesign.Models
{
    public class Box
    {
        public int Id { get; set; }
        public int BoxOrderId { get; set; }
        public string ThemeColor { get; set; }
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public string DescriptionText { get; set; }
        public string Features { get; set; }
        public string BulletImageUrl { get; set; }
        public string SummaryText { get; set; }
        public string buttonText { get; set; }
        public string optionIcon { get; set; }
        public string EnquiryTypeButton { get; set; }
        public List<OptionIcon> optionIcons { get; set; } = new List<OptionIcon>();
    }
}