﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco_Redesign.Models
{
    public class SectionWithSlidesModel
    {
        public string slideLogoUrl { get; set; }
        public int slideId { get; set; }
        public object slideContent { get; set; }

    }
}