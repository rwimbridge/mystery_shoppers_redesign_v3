﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco_Redesign.Models
{
    public class SectionWithModalWindowsModel
    {
        public string modalHeader { get; set; }
        public string modalId { get; set; }
        public string modalImageUrl { get; set; }
        public string modalVideoUrl { get; set; }
        public object modalContent { get; set; }
        public string modalBadge { get; set; }
        public string modalHeaderColour { get; set; }
    }
}