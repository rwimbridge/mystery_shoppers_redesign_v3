﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco_Redesign.Models
{
    public class ExampleContent
    {
        public string IconUrl { get; set; }
        public string IconType { get; set; }
        public string Title { get; set; }
        public string DemoButtonText { get; set; }
    }
}