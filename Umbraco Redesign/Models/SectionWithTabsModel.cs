﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;

namespace Umbraco_Redesign.Models
{
    public class SectionWithTabsModel
    {
        public IPublishedContent node { get; set; }
        public string tabHeader { get; set; }
        public object tabContent { get; set; }
        public int    tabsId { get; set; }
        public string tabId { get; set; }
        public string tabLayout { get; set; }
        public string tabAlias { get; set; }
    }
}