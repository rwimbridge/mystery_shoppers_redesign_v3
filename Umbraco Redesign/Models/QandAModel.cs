﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;

namespace Umbraco_Redesign.Models
{
    public class QandAModel
    {
        public string questionUrl { get; set; }
        public string questionId { get; set; }
        public string questionText { get; set; }
        public string answerText { get; set; }
        public List<ContentLinkModel> answerOptions { get; set; }
        public string questionImageUrl { get; set; }
        public string questionImageLowResUrl { get; set; }
        public string buttonColor { get; set; }
        public string questionType { get; set; }
        public string numberOfQuestions { get; set; }
        public int questionAndAnswerId { get; set; }
    }
}