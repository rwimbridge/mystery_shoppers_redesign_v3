﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.Models;

namespace Umbraco_Redesign.Models
{
    public class AnswerModel
    {
        public string answerTitle { get; set; }
        public string answerContent { get; set; }
        public IEnumerable<Link> answerLinks { get; set; }
    }
}