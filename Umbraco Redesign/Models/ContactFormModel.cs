﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Umbraco_Redesign.Models
{
    public class ContactFormModel
    {
        public string selectedItem { get; set; }

        [Required]
        public string Name { get; set; }
        public string EnquiryType { get; set; }
        [Required]
        public string Company { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Message { get; set; }
        public int PageId { get; set; }
    }
}