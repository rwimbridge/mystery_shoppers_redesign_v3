﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;

namespace Umbraco_Redesign.Models
{
    public class SectionsModel
    {
        public IPublishedContent page { get; set; }
        public string pageId { get; set; }
        public bool isHomePage { get; set; }
        public string pageName { get; set; }
        public string sectionId { get; set; }
        public string backgroundColor { get; set; }
        public string textColor { get; set; }
        public string classes { get; set; }

        public List<SectionsModel> lstSections { get; set; }

    }
}