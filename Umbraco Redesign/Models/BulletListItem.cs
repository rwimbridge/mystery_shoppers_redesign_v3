﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco_Redesign.Models
{
    public class BulletListItem
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}