﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;

namespace Umbraco_Redesign.Models
{
    public class IconAndTextModel
    {
        public string CompanyValueId { get; set; }
        public string CompanyValueTextArea { get; set; }
        public string CompanyValueTitle { get; set; }
        public string IconPicker { get; set; }
        public string IconPickerUrl { get; set; }
    }
}
