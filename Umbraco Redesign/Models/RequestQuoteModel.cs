﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Umbraco.Core.Models;

namespace Umbraco_Redesign.Models
{
    public class RequestQuoteModel
    { public string QuoteId { get; set; }
        public string nameLabel { get; set; } //needed
        [Required]
        public string name { get; set; }   //needed
        [Required]
        public string phone { get; set; }  //needed
        public string message { get; set; }//needed
        public string howHearAbout { get; set; } //needed
        public string other { get; set; } //needed
        public List<string> howDropDown { get; set; } //needed
        [Required]
        public string jobTitle { get; set; } //needed
        [Required, EmailAddress]
        public string email { get; set; }//needed
        [Required]
        public string companyName { get; set;} //needed
        public bool callBack { get; set; } //needed
        public bool emailBack { get; set; }//needed
        public string emailOkLabel { get; set; } //needed
        public string phoneOkLabel { get; set; }//needed
        public bool okToAnnounce { get; set; }
        [Required]
        public string mainObjectives { get; set;} //needed
        public bool selected { get; set; }
        public List<checkListModel> lstCheck { get; set; }        
    }

    public class checkListModel
    {
        public int ID { get; set; }
        public string name { get; set; }
        public bool selected { get; set; }
    }





}