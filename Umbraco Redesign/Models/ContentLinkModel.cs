﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco_Redesign.Models
{
    public class ContentLinkModel
    {
        public string contentName { get; set; }
        public string contentTarget { get; set; }
        public string contentUrl { get; set; }
    }
}