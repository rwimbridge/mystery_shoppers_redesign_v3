﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco_Redesign.Models
{
    public class OfficesModel
    {
        //public List<OfficeProperty> officeProperties{ get; set; }
        //public string officeName { get; set; }
        public string officeFlag { get; set; }
        public string officeName { get; set; }
        public string officeAddressName { get; set; }
        public string officeAddressLine1 { get; set; }
        public string officeAddressLine2 { get; set; }
        public string officeAddressLine3 { get; set; }
        public string officeAddressLine4 { get; set; }
        public string officeTown { get; set; }
        public string officeCounty { get; set; }
        public string officePostcode { get; set; }
        public string officePhone { get; set; }
        public string officeEmail { get; set; }
        public string extraHeading { get; set; }
        public string extraHeadingContent { get; set; }
        public string mobilePhone { get; set; }
        public string extraPhone { get; set; }
        public string extraEmail { get; set; }
        public string officeWebsite { get; set; }
        public string officeCountryCode { get; set; }
        public string RegisteredAddressLine1 { get; set; }
        public string RegisteredAddressLine2 { get; set; }
        public string RegisteredAddressLine3 { get; set; }
        public string RegisteredTown { get; set; }
        public string RegisteredPostcode { get; set; }
        public string RegisteredPhone { get; set; }
        public string showInOfficeMenu { get; set; }
        public string orContactUs { get; set; }
        public string orPressOne { get; set; }
        public string findOutHow { get; set; }
        public string officeFlagUrl { get; set; }
        public string shopperSupportTeam { get; set; }
        public string officeOrderNumber { get; set; }
    }
}