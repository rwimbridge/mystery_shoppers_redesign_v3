﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco_Redesign.Models
{
    public class SectionWithCarouselModel
    {
        public string slideLogoUrl { get; set; }
        public string slideId { get; set; }
        public object slideContent { get; set; }
        public int carouselId { get; set; }
        public bool FullWidth { get; set; }
    }
}