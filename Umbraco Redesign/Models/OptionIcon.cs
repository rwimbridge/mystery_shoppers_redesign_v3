﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco_Redesign.Models
{
    public class OptionIcon
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string image { get; set; }
        public string leftBoxContent { get; set; }
        public string leftExampleLink { get; set; }
        public string middleBoxContent { get; set; }
        public string middleExampleLink { get; set; }
        public string rightBoxContent { get; set; }
        public string rightExampleLink { get; set; }
    }
}