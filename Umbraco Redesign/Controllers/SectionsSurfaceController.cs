﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco_Redesign.Models;

namespace Umbraco_Redesign.Controllers
{
    public class SectionsSurfaceController : SurfaceController
    {
        /// <summary>
        /// This is the main Index Method that is loaded when the page loads 
        /// which gets the node of the current page loaded and then returns the 
        /// Sections partial view to display the content
        /// </summary>
        /// <returns>Sections Partial View</returns>
        public ActionResult Index()
        {
            IPublishedContent sectionIdNode = CurrentPage;

            int pageId = -1;

            if (sectionIdNode != null)
            {
                var parent = sectionIdNode.Parent;

                if (parent != null)
                {
                    if (parent.DocumentTypeAlias.Equals("homePage"))
                    {
                        pageId = sectionIdNode.Id;
                    }
                    else
                    {
                        pageId = sectionIdNode.Parent.Id;
                    }
                }
                else
                {
                    pageId = sectionIdNode.Id;
                }
            }
            TempData["pageId"] = pageId.ToString();

            List<SectionsModel> sectionModel =  GetSections(pageId.ToString());

            sectionModel = new List<SectionsModel>();
            return PartialView("sections", sectionModel);
        }

        /// <summary>
        /// Uses the passed in sectionId to get the content of the Ipublished content page 
        /// and any sections that page might have
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns>Sections Partial View</returns>
        public ActionResult GetSinglePage(string sectionId)
        {

            IPublishedContent sectionIdNode = Umbraco.TypedContent(Convert.ToInt32(sectionId));

            int pageId = -1;

            if(sectionIdNode != null)
            {
                var parent = sectionIdNode.Parent;

                if(parent != null)
                {
                    if(parent.DocumentTypeAlias.Equals("homePage") || parent.DocumentTypeAlias.ToLower().Contains("menu"))
                    {
                        pageId = sectionIdNode.Id;
                    }
                    else
                    {
                        pageId = sectionIdNode.Parent.Id;
                    }
                }
                
            }

            TempData["pageId"] = pageId.ToString();

            List<SectionsModel> sectionModel = GetSections(pageId.ToString());

            return PartialView("sections", sectionModel);

        }

        [HttpPost]
        public ActionResult GetIdFromTitle(string title)
        {
            if (title == string.Empty)
            {
                return Json("-1");
            }

            var article = Umbraco.TypedContentAtRoot().FirstOrDefault().Descendants().Where(a => a.Name.ToLower().Trim().Replace("/", "").Replace("'", "") == title.ToLower().Trim().Replace("/","").Replace("'","")).FirstOrDefault();

            if(article != null)
            {
                var articleAlias = article.DocumentTypeAlias;

                var modalImage = String.Empty;
                var modalHeader = String.Empty;
                var modalImageUrl = String.Empty;
                var modalVideoUrl = String.Empty;
                var modalContent = String.Empty;

                switch (articleAlias)
                {
                    case "modalWindow":
                        modalHeader = article.GetPropertyValue<string>("mSLModalTitle");
                        modalImageUrl = Umbraco.TypedMedia(modalImage) != null ? Umbraco.TypedMedia(modalImage).Url : null; ;
                        modalVideoUrl = article.GetPropertyValue<string>("mSLModal");
                        modalContent = article.GetPropertyValue<string>("mSLModalText");
                    break;
                    case "questionAndAnswer":
                        modalHeader = article.GetPropertyValue<string>("mSLQuestionText");
                        modalImageUrl = null;
                        modalVideoUrl = null;
                        var articleAnswer = article.GetPropertyValue<string>("mSLAnswerText");

                        // if there are answer links get the links
                        IEnumerable<Link> answerOptions = article.GetPropertyValue<IEnumerable<Link>>("mSLAnswerOptions");

                        var answerLinks = String.Empty;
                        answerLinks += "<ul class='flip-card-links'>";
                        foreach(var option in answerOptions)
                        {
                            answerLinks += "<li><a class='card-link clickable' target='" + option.Target + "' href='" + option.Url + "'>" + option.Name + "</a></li>";
                        }
                        
                        answerLinks += "</ul>";

                        modalContent = "<div>" + articleAnswer + "</div>" + "<div>" + answerLinks + "</div>";
                        break;
                    default:
                        return Json("-1");
                        break;
                }


                var modalArticle = new SectionWithModalWindowsModel
                {
                    modalHeader = modalHeader,
                    modalImageUrl = modalImageUrl,
                    modalVideoUrl = modalVideoUrl,
                    modalContent = modalContent
                };

                if(modalArticle != null)
                {
                    return Json(modalArticle);
                }

                
            }

            return Json("-1");



        }

        /// <summary>
        /// Uses the browser address bar page title passed from jquery to get the Ipublished content Id
        /// Where the page name equals that page title
        /// </summary>
        /// <param name="pageTitle"></param>
        /// <returns>page Id</returns>
        public string GetSectionId(string pageTitle)
        {
            Regex pattern = new Regex("[?#//]");
            IPublishedContent page = null;
            IPublishedContent pageName = null;
            string replacePageName = String.Empty;
            var sectionId = "-1";

            

            if(pageTitle == "")
            {

                page = Umbraco.TypedContentAtRoot().FirstOrDefault();
            }
            else
            {
                pageName = Umbraco.TypedContentAtRoot().FirstOrDefault().Descendants().Where(x => x.Name.ToLower().Replace("-", " ") == pageTitle && !x.DocumentTypeAlias.ToLower().Contains("menu")).FirstOrDefault();
                if (pageName != null)
                {
                    replacePageName = pattern.Replace(pageName.Name, "");
                    pageTitle = replacePageName;
                    page = Umbraco.TypedContentAtRoot().FirstOrDefault().Descendants().Where(x => pattern.Replace(x.Name.ToLower(), "").Replace("-", " ") == pageTitle.ToLower() && !x.DocumentTypeAlias.ToLower().Contains("menu")).FirstOrDefault();

                    
                }
            }


            //if (pageName != null)
            //{
            //    if(pageTitle == "")
            //    {
                    
            //    }
            //    else
            //    {
            //        replacePageName = pattern.Replace(pageName.Name, "");
            //        pageTitle = replacePageName;
            //        page = Umbraco.TypedContentAtRoot().FirstOrDefault().Descendants().Where(x => x.Name.ToLower() == pageTitle.ToLower()).FirstOrDefault();
            //    }
                
                
            //}

            //page = Umbraco.TypedContentAtRoot().FirstOrDefault().Descendants().Where(x =>x.Name.ToLower() == pageTitle.ToLower()).FirstOrDefault();
           

            if(page != null && page.DocumentTypeAlias != "homePage")
            {
                if (page.DocumentTypeAlias.ToLower() == "modalwindow")
                {
                    sectionId = "#section" + page.Ancestors().Where(p =>p.DocumentTypeAlias == "singlePage").FirstOrDefault().Id.ToString();
                }
                else
                {
                    sectionId = "#section" + page.Id;
                }


                
            }

            return sectionId;
        }

        /// <summary>
        /// This uses the passed in pageId to check if there are any sections underneath 
        /// the page node it then sets the properties of a temporary SectionsModel and then 
        /// adds each temporary model to the main model list and then returns the main model 
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        private List<SectionsModel> GetSections(string pageId)
        {
            var model = new SectionsModel();
            model.lstSections = new List<SectionsModel>();
            var lstSections = new List<SectionsModel>();
            var tempModel = new SectionsModel();
            var isHomePage = false;

            IPublishedContent page = null;
            page = Umbraco.TypedContent(pageId);

            IEnumerable<IPublishedContent> sections = null;

            if(page != null)
            {
                if (page.DocumentTypeAlias == "homePage")
                {
                    isHomePage = true;
                    sections = page.Descendants().Where(x => x.IsVisible() && x.DocumentTypeAlias.ToString().Contains("section") && Convert.ToBoolean(x.GetPropertyValue("mSLShowOnHomePage")) == true).OrderBy(x => x.GetPropertyValue<int>("mSLSectionOrderNumber"));
                }
                else
                {
                    sections = page.Descendants().Where(x => x.IsVisible() && x.DocumentTypeAlias.ToString().Contains("section")).OrderBy(x => x.GetPropertyValue<int>("mSLSectionOrderNumber"));
                }

                if(sections.Count() > 0)
                {
                    int sectionCount = 0;

                    foreach(var mslSection in sections)
                    {
                        tempModel = new SectionsModel();

                        var sectionId = "section" + mslSection.Id;
                        var hasBackground = Convert.ToBoolean(mslSection.GetPropertyValue("mSLHasBackgroundColor"));
                        string backgroundColor = "none";
                        string textColor = String.Empty;
                        string classes = String.Empty;

                        if (Convert.ToBoolean(mslSection.GetPropertyValue("mSLShowOnHomePage")) == true)
                        {
                            classes = "sectionContentWrapper onlyHomePageSection home-page";
                        }
                        else
                        {
                            classes = "sectionContentWrapper";
                        }

                        if (hasBackground)
                        {
                            textColor = "#000";

                            if (mslSection.GetPropertyValue("mSLSectionBackgroundColor") != null)
                            {
                                backgroundColor = "#" + mslSection.GetPropertyValue<string>("mSLSectionBackgroundColor");
                            }
                        }
                        else
                        {
                            textColor = "#fff";
                        }

                        // Add To The Model
                        tempModel = AssignPropertiesToModel(mslSection, pageId, isHomePage, sectionId, backgroundColor, textColor, classes);

                        //Add to the sections list
                       AddModelToSectionModelList(tempModel, lstSections);

                    }
                }
                else
                {
                    var hasBackground = Convert.ToBoolean(page.GetPropertyValue("mSLHasBackgroundColor"));
                    var backgroundColor = "#" + page.GetPropertyValue("mSLSectionBackgroundColor");
                    var textColor = String.Empty;

                    if (hasBackground)
                    {
                        textColor = "#000";
                    }
                    else
                    {
                        textColor = "#fff";
                    }

                    tempModel = AssignPropertiesToModel(page, pageId, isHomePage, "", backgroundColor, textColor, "");

                    //Add to the sections list
                    AddModelToSectionModelList(tempModel, lstSections);
                }



            }


            return lstSections;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page">Ipublished Content in the Umbraco system</param>
        /// <param name="pageId">Id of the page</param>
        /// <param name="isHomePage">Whether it is meant to be displayed on the home page</param>
        /// <param name="sectionId">The Id of the section under the page</param>
        /// <param name="backgroundColor">Whether of not the section has a background color or not</param>
        /// <param name="textColor">The color of the text to display</param>
        /// <param name="classes">which classes should be applied to this section</param>
        /// <returns>SectionModel which the above paraeters applied</returns>
        private SectionsModel AssignPropertiesToModel(IPublishedContent page, string pageId, bool isHomePage, string sectionId, string backgroundColor, string textColor, string classes)
        {
            var sectionModel = new SectionsModel();

            sectionModel.page = page;
            sectionModel.pageId = pageId;
            sectionModel.isHomePage = isHomePage;
            sectionModel.sectionId = sectionId;
            sectionModel.backgroundColor = backgroundColor;
            sectionModel.textColor = textColor;
            sectionModel.classes = classes;

            return sectionModel;
        }

        /// <summary>
        /// This method Add the tempModel SectionModel to the original SectionsModel list 
        /// </summary>
        /// <param name="tempModel">A temporary instance of SectionsModel with the properties</param>
        /// <param name="orginalSectionsModel">The original SectionsModel instance</param>
        private void AddModelToSectionModelList(SectionsModel tempModel, List<SectionsModel> orginalSectionsModel)
        {

            if (tempModel != null)
            {
                orginalSectionsModel.Add(tempModel);
            }

            //return orginalSectionsModel;
        }

    }
}