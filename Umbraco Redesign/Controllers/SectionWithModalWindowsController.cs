﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Extensions;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco_Redesign.Models;

namespace Umbraco_Redesign.Controllers
{
    public class SectionWithModalWindowsController : SurfaceController
    {
        // GET: SectionWithModalWindows
        public ActionResult Index( int modalsId )
        {
            var sectionWithModalWindowsModel = GetSectionWithModalWindows(modalsId);
            return PartialView("solutions", sectionWithModalWindowsModel);
        }

        public List<SectionWithModalWindowsModel> GetSectionWithModalWindows(int modalsId)
        {
            string modalHeader = String.Empty;
            string modalId = String.Empty;
            string modalImageUrl = String.Empty;
            string modalVideoUrl = String.Empty;
            string modalBadge = String.Empty;
            string modalHeaderColour = String.Empty;
            object modalImage = null;
            object modalContent = null;
            object modalVideo = null;
            List<SectionWithModalWindowsModel> modalsList = new List<SectionWithModalWindowsModel>();

            IPublishedContent modalsNode = Umbraco.TypedContentAtRoot().FirstOrDefault().Descendants().Where(x => x.Id == Convert.ToInt32(modalsId) && x.IsVisible()).FirstOrDefault();

            if (modalsNode != null)
            {
                foreach (IPublishedContent modal in modalsNode.Children())
                {
                    var sectionWithModalWindowsModel = new SectionWithModalWindowsModel();
                    modalHeader = modal.GetPropertyValue<string>("mSLModalTitle");
                    modalId = modal.Id.ToString();
                    modalImage = modal.GetPropertyValue<string>("mSLModalImage");
                    if(modalImage != null)
                    {
                        var fullImageUrl = Umbraco.TypedMedia(modalImage);
                        var extension = fullImageUrl.GetPropertyValue<string>("umbracoExtension");

                        modalImageUrl = Umbraco.TypedMedia(modalImage).Url;
                        //modalImageUrl = modalImageUrl.Replace("." + extension, "");
                        //modalImageUrl = modalImageUrl + "_preview" + "." + extension;


                        //modalImageUrl = Umbraco.TypedMedia(modalImage).Url;
                    }
                    
                    modalVideo = modal.GetPropertyValue<string>("mSLModal");
                    modalContent = modal.GetPropertyValue("mSLModalText");
                    modalBadge = modal.GetPropertyValue<string>("badgeName");
                    modalHeaderColour = modal.GetPropertyValue<string>("colourPickerForHeader");
                    

                    //if (modalVideo != null)
                    //{
                    //    modalVideoUrl = Umbraco.TypedMedia(modalVideo).Url;
                    //}

                    if (modalHeader != "" && modalContent != null)
                    {
                        sectionWithModalWindowsModel.modalHeader = modalHeader;
                        sectionWithModalWindowsModel.modalId = modalId;
                        sectionWithModalWindowsModel.modalImageUrl = modalImageUrl;
                        sectionWithModalWindowsModel.modalContent = modalContent;
                        sectionWithModalWindowsModel.modalBadge = modalBadge;
                        sectionWithModalWindowsModel.modalHeaderColour = modalHeaderColour;

                        //if (modalVideoUrl != null)
                        //{
                        //    sectionWithModalWindowsModel.modalVideoUrl = modalVideoUrl;
                        //}

                        modalsList.Add(sectionWithModalWindowsModel);
                    }
                }
            }
            return modalsList;
        }
    }
}