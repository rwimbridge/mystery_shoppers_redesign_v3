﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Extensions;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco_Redesign.Models;

namespace Umbraco_Redesign.Controllers
{
    public class BulletListController : SurfaceController
    {
        // GET: BulletList
        public ActionResult Index(int nodeId)
        {
           
            var iconList = GetIconList(nodeId); 
            return PartialView("BulletList", iconList);
        }

        public ActionResult GetBulletListItemContent(int nodeId)
        {
            IPublishedContent bulletListItemNode = Umbraco.TypedContent(nodeId);

            if(bulletListItemNode != null)
            {
                var bulletListItemTitle = bulletListItemNode.Name;
                var bulletListItemContent = bulletListItemNode.GetPropertyValue<string>("listItemDescription");

                var bulletListItem = new BulletListItem
                {
                    Title = bulletListItemTitle,
                    Content = bulletListItemContent
                };

                return bulletListItem != null ? Json(bulletListItem) : Json("-1");

            }

            return Json("-1");
        }

        public List<BulletList> GetIconList(int nodeId)
        {
            IPublishedContent node = Umbraco.TypedContent(nodeId);
            var bulletList = new List<BulletList>();
            foreach(var bullet in node.Children().Where(li =>li.IsVisible()))
            {
                var icon = bullet.GetProperty("iconPicker").DataValue;
                var json = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(icon.ToString());
                var listItem = new BulletList
                {
                    Id = bullet.Id,
                    Contenturl = bullet.Url,
                    IconName = json["className"],
                    ListItemDescription = bullet.GetPropertyValue<string>("listItemDescription"),
                    Title = bullet.Name

                };
                if(listItem != null)
                {
                    bulletList.Add(listItem);
                }
            }
            return bulletList;
        }
    }
}