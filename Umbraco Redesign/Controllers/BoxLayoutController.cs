﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;
using Umbraco_Redesign.Models;
using Umbraco.Web;
using HtmlAgilityPack;

namespace Umbraco_Redesign.Controllers
{
    public class BoxLayoutController : SurfaceController
    {
        // GET: BoxLayout
        public ActionResult Index(int? tabId)
        {
            if(Request.Params["tab"] != null && Request.Params["client"] != null && Request.Params["version"] != null)
            {
                var tab = Request.Params["tab"];
                var privateReporting = Umbraco.TypedContentAtRoot().FirstOrDefault().Descendants().Where(t => t.Name.ToLower()
                .Contains("summary") && t.Name.ToLower().Contains("reporting") && t.Name.ToLower().Contains("clients")).FirstOrDefault();

                if(privateReporting != null)
                {
                    tabId = privateReporting.Id;
                }

            }


            var tabNode = Umbraco.TypedContent(tabId);


            var boxes = GetBoxes(tabNode);

            if(boxes.Count() > 0)
            {
                return PartialView("boxLayout", boxes);
            }

            return CurrentUmbracoPage();
        }

        private List<Box> GetBoxes(IPublishedContent tabNode)
        {
            var boxes = new List<Box>();
            var pricingBoxes = tabNode.Descendants().Where(t => t.DocumentTypeAlias == "pricingBox").ToList();

            if (pricingBoxes.Count() == 0)
                return boxes;

            for (var tabCount = 0; tabCount < 3; tabCount++)
            {
                var tab = pricingBoxes[tabCount];


                //foreach (var tab in tabNode.Descendants().Where(t => t.DocumentTypeAlias=="pricingBox").ToList())
                //{

                    var boxOrderId = tab.GetPropertyValue<string>("pricingBoxNumber") != String.Empty ? tab.GetPropertyValue<string>("pricingBoxNumber"): String.Empty;
                    var themeColor = tab.GetPropertyValue<string>("pricingBoxThemeColorPicker");
                    var boxImage = tab.GetPropertyValue<string>("pricingBoxImage");
                    var imageUrl = Umbraco.TypedMedia(boxImage.ToString()) != null ? Umbraco.TypedMedia(boxImage.ToString()).Url : String.Empty;
                    var boxTitle = tab.GetPropertyValue<string>("pricingBoxTitleText");
                    var boxDescription = tab.GetPropertyValue<string>("pricingBoxDescription");
                    var features = tab.GetPropertyValue<string>("pricingBoxFeaturesList");
                    var tickBoxImage = tab.GetPropertyValue<string>("pricingBoxBulletListImage");
                    var listImage = "<li class='bulletListImage' style='list-style-type: square'>";
                    var tickBoxImageUrl = Umbraco.TypedMedia(tickBoxImage.ToString()) != null ? Umbraco.TypedMedia(tickBoxImage.ToString()).Url : String.Empty;
                    if (tickBoxImageUrl != String.Empty)
                    {
                        listImage = "<li class='bulletListImage' style='background-image: url(" + tickBoxImageUrl + ")'>";
                    }

                    features = features.Replace("<li>", listImage);
                    var boxSummaryText = tab.GetPropertyValue<string>("pricingBoxSummaryText");
                    var boxButtonText = tab.GetPropertyValue<string>("pricingBoxButtonText");
                    var enquiryButtonText = tab.GetPropertyValue<string>("pricingBoxButtonEnquiryType");

                    var optionIcons = tab.GetPropertyValue<IEnumerable<string>>("pricingBoxOptionIcons");
                    var selectedOptionIcons = new List<OptionIcon>();

                    foreach(var icon in optionIcons)
                    {
                        var optionalIcons = tabNode.Descendants().Where(tn => tn.DocumentTypeAlias == "optionIcons").FirstOrDefault();
                        var optionIconNode = optionalIcons != null? optionalIcons.Children().Where(t => t.Name.ToLower().Contains(icon.ToLower())).FirstOrDefault(): null;

                        if(optionIconNode != null)
                        {
                            var title = optionIconNode.GetPropertyValue<string>("iconExampleTitle");
                            var iconImage = Umbraco.TypedMedia(optionIconNode.GetPropertyValue("iconImage").ToString());
                            var leftBoxContent = Umbraco.TypedMedia(optionIconNode.GetPropertyValue("iconExampleContentLeftBox").ToString());
                            var leftBoxExampleLink = optionIconNode.GetPropertyValue<string>("iconExampleContentLeftLink");
                            var middleBoxContent = Umbraco.TypedMedia(optionIconNode.GetPropertyValue("iconExampleContentMiddleBox").ToString());
                            var middleBoxExampleLink = optionIconNode.GetPropertyValue<string>("iconExampleContentMiddleLink");
                            var rightBoxContent = Umbraco.TypedMedia(optionIconNode.GetPropertyValue("iconExampleContentRightBox").ToString());
                            var rightBoxExampleLink = optionIconNode.GetPropertyValue<string>("iconExampleContentRightLink");

                        var optionIcon = new OptionIcon
                        {
                                Id = Convert.ToInt32(optionIconNode.Id),
                                title = optionIconNode.GetPropertyValue<string>("iconExampleTitle"),
                                image = iconImage != null ? iconImage.Url : String.Empty,
                                leftBoxContent = leftBoxContent != null? leftBoxContent.Url: String.Empty,
                                leftExampleLink = leftBoxExampleLink,
                                middleBoxContent = middleBoxContent != null? middleBoxContent.Url: String.Empty,
                                middleExampleLink = middleBoxExampleLink,
                                rightBoxContent = rightBoxContent != null? rightBoxContent.Url: String.Empty,
                                rightExampleLink = rightBoxExampleLink
                            };

                            if(optionIcon != null)
                            {
                                selectedOptionIcons.Add(optionIcon);
                            }
                        }

                        
                    }

                    var box = new Box
                    {
                        Id = tabCount,
                        BoxOrderId = Convert.ToInt32(boxOrderId),
                        ThemeColor = themeColor,
                        ImageUrl = imageUrl,
                        Title = boxTitle,
                        DescriptionText = boxDescription,
                        Features = features,
                        BulletImageUrl = tickBoxImageUrl,
                        SummaryText = boxSummaryText,
                        buttonText = boxButtonText,
                        optionIcons = selectedOptionIcons,
                        EnquiryTypeButton = enquiryButtonText

                    };

                    if(box != null)
                    {
                        boxes.Add(box);
                    }


                    //var box = new Box
                    //{
                    //    BoxOrderId = Convert.ToInt32(tab.GetPropertyValue<int>("pricingBoxNumber")),
                    //    ThemeColor = tab.GetPropertyValue<string>("pricingBoxThemeColorPicker"),
                    //    ImageUrl = tab.GetPropertyValue<string>("")


                    //};

                    //if (box != null)
                    //{
                    //    boxes.Add(box);
                    //}
                    
                //}    
            }

            return boxes;
        }

        public ActionResult GetIconContent(string iconName, string boxNumber)
        {
            string boxName = String.Empty;

            switch (boxNumber)
            {
                case "1":
                    boxName = "Left";
                    break;
                case "2":
                    boxName = "Middle";
                    break;
                case "3":
                    boxName = "Right";
                    break;
            }

            var root = Umbraco.TypedContentAtRoot().First();
            var demoButtonOptions = root.Descendants().Select(x => x).Where(x => x.DocumentTypeAlias == "pricing").FirstOrDefault();
            string demoButtonText = String.Empty;

            if (demoButtonOptions != null)
            {
                demoButtonText = demoButtonOptions.GetPropertyValue<string>("demoRequestDemoText");
            }

            var iconContent = root.Descendants().Where(x => x.DocumentTypeAlias == "optionIcon" && x.Id.ToString() == iconName).FirstOrDefault();

            IPublishedContent icon = null;
            string iconUrl = String.Empty;
            string iconTitle = String.Empty;
            string iconType = String.Empty;

            if (iconContent != null)
            {
                var test = iconContent.GetPropertyValue<string>("IconExampleContent" + boxName + "Link");

                if (iconContent.GetPropertyValue<string>("IconExampleContent" + boxName + "Link") != String.Empty)
                {
                    //icon = (IPublishedContent)iconContent.GetPropertyValue("IconExampleContent" + boxName + "Link");
                    iconUrl = iconContent.GetPropertyValue<string>("IconExampleContent" + boxName + "Link");
                    iconType = "link";
                }
                else
                {
                    icon = Umbraco.TypedMedia(iconContent.GetPropertyValue("iconExampleContent" + boxName + "Box").ToString());
                    iconUrl = icon.Url;
                    iconType = icon.GetPropertyValue<string>("umbracoExtension");
                }

                if (icon != null || iconUrl != String.Empty)
                {
                    var exampleContent = new ExampleContent
                    {
                        IconUrl = iconUrl,
                        IconType = iconType,
                        Title = iconContent.GetPropertyValue<string>("iconExampleTitle"),
                        DemoButtonText = demoButtonText
                    };

                    return Json(exampleContent);
                }
            }

            return null;

        }
    }
}