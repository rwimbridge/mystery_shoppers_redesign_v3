﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Umbraco_Redesign.Models;
using Umbraco.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace Umbraco_Redesign.Controllers
{
    public class RequestQuoteSurfaceController : SurfaceController
    {
        // GET: RequestQuote
        public ActionResult Index(string sectionId)
        {
            var requestaquotemodel = GetRequestQuotes(sectionId);
            return PartialView("QuoteForm", requestaquotemodel);
        }
        public RequestQuoteModel GetRequestQuotes(string sectionId)
        {
            RequestQuoteModel model = new RequestQuoteModel();
            model.lstCheck = null;
            List<checkListModel> lst = new List<checkListModel>();
            var dropdownList = new List<string>();
            //dropdownList.Add("howDidYouHearAboutUsList");

            IPublishedContent RequestQuoteNode = Umbraco.TypedContentAtRoot().FirstOrDefault().Descendants().Where(x => x.Id == Convert.ToInt32(sectionId) && x.IsVisible()).FirstOrDefault();

            if (RequestQuoteNode != null)
            {
               
                foreach (IPublishedContent dropdown in RequestQuoteNode.Children())
                {
                    dropdownList.Add("howDidYouHearAboutUsList");
                    var itemCount = 0;
                    var tempList = new List<string>();
                    var dropDownOptionsList = new List<SelectListItem>();
                    var prevAlias = "";

                    //var propertyAliasValue = CurrentPage.GetProperty(dropdown).Value.ToString();
                    var propertyAliasValue = dropdown.GetPropertyValue<string>("howDidYouHearAboutUsList");
                    var pageHeading = dropdown.GetPropertyValue<string>("pageHeadingDetails");
                    tempList = propertyAliasValue.Split(',').ToList();

                    foreach (var item in tempList)
                    {
                        //if (dropdown == "whatServicesList")
                        //{

                        //    lst.Add(new checkListModel { ID = itemCount, name = item, selected = false });
                        //}
                        var propertyAlias = dropdown.GetProperty("howDidYouHearAboutUsList").PropertyTypeAlias;
                        dropDownOptionsList.Insert(itemCount, new SelectListItem { Text = item, Value = item });
                        prevAlias = propertyAlias;
                        if (propertyAlias != prevAlias || itemCount == 0)
                        {
                            TempData[propertyAlias] = dropDownOptionsList as IEnumerable<SelectListItem>;
                        }

                        itemCount++;
                    }
                 
                }
                if (lst != null)
                {
                    model.lstCheck = lst;
                }
               
            }
            return model;
        }
        [HttpPost]
        public ActionResult HandleQuoteSubmit(RequestQuoteModel model)
        {
            if(!ModelState.IsValid)
            {
                model.callBack = false;
                model.emailBack = false;
                model.okToAnnounce = false;
                return CurrentUmbracoPage();
            }
            string response = Request["g-recaptcha-response"];

            var recaptchaValidations = Recaptcha.ValidateCaptcha(response);
            recaptchaValidations = true;
            if (!recaptchaValidations)
            {
                ViewData["invalid"] = "Invalid Captcha";

                return CurrentUmbracoPage();
            }
            var selectedServices = checkSelected(model);
            StringBuilder sb = new StringBuilder();

            var dateToday = DateTime.Now.ToString("dd-MM-yyyy");
            var timeToday = DateTime.Now.ToLocalTime();

            var type = model.GetType();
            var properties = type.GetProperties();

           
            Dictionary<string, string> emailProperties = new Dictionary<string, string>();
            var itemcount = 0;
            string body = "";

            foreach (var property in properties)
            {
                if (property != null)
                {
                    var result = property.GetValue(model, null);
                    if (result != null)
                    {
                        emailProperties.Add(property.Name, result.ToString().Trim());

                    }

                    itemcount++;

                }

            }
            TempData["properties"] = sb.ToString();

            // Email
            IPublishedContent RequestQuoteNode = Umbraco.TypedContentAtRoot().FirstOrDefault().Descendants().Where(x => x.DocumentTypeAlias == "requestAQuote" && x.IsVisible()).FirstOrDefault();

            MailDefinition md = new MailDefinition();

            // Add as BCC all other recipeints
            var allRecipients = RequestQuoteNode.GetProperty("emailTo");
            var str = allRecipients.Value;

            md.From = "no-reply@mystery-shoppers.co.uk";
            md.CC = "ivan.ivanov@mystery-shoppers.co.uk";
            //emailTo = md.From.ToString();
            var emailTo = str.ToString();

            md.IsBodyHtml = true;
            var subject = "A Quotation Request From " + Request.Url.Host;
            md.Subject = subject;

            var nameLabel = RequestQuoteNode.GetProperty("nameLabel").Value.ToString();
            var jobTitleLabel = RequestQuoteNode.GetProperty("jobTitleLabel").Value;
            var companyLabel = RequestQuoteNode.GetProperty("companyLabel").Value;
            var phoneLabel = RequestQuoteNode.GetProperty("companyTelephoneNumber").Value.ToString();
            var emailLabel = RequestQuoteNode.GetProperty("emailLabel").Value;
            var howHearLabel = RequestQuoteNode.GetProperty("howDidYouHearAboutUsLabel").Value;
            var messageLabel = RequestQuoteNode.GetProperty("yourObjectiveEnquiry").Value;
            var callBackLabel = RequestQuoteNode.GetProperty("pleaseCallMeBackInstructions").Value;
            var emailBackLabel = RequestQuoteNode.GetProperty("pleaseEmailMeInstructions").Value;
            var ojectivesLabel = RequestQuoteNode.GetProperty("yourObjectiveEnquiry").Value;

            if (emailProperties.ContainsKey("name"))
            {
                sb.Append(nameLabel + ": " + "<b>" + emailProperties["name"] + "</b>" + "<br />");
            }
            
            if (emailProperties.ContainsKey("jobTitle"))
            {
                sb.Append(jobTitleLabel + ": " + "<b>" + emailProperties["jobTitle"] + "</b>" + "<br />");
            }

            if (emailProperties.ContainsKey("companyName"))
            {
                sb.Append(companyLabel + ": " + "<b>" + emailProperties["companyName"] + "</b>" + "<br />");
            }

            if (emailProperties.ContainsKey("phone"))
            {

                sb.Append(phoneLabel + ": " + "<b>" + emailProperties["phone"] + "</b>" + "<br />");
            }

            if (emailProperties.ContainsKey("email"))
            {
                sb.Append(emailLabel + ": " + "<b>" + emailProperties["email"] + "</b>" + "<br />");
            }
            
            if (emailProperties.ContainsKey("howHearAbout"))
            {
                sb.Append(howHearLabel + ": " + "<b>" + emailProperties["howHearAbout"] + "</b>" + "<br />");
            }

            if (emailProperties.ContainsKey("mainObjectives"))
            {
                sb.Append("<p>");
                sb.Append(messageLabel + ": " + "<b>" + emailProperties["mainObjectives"] + "</b>");
                sb.Append("</p>");
            }

            body = "<p>A Quotation has been received from " + "<b>" + Request.Url.Host + "</b>" + "</p>" +
              "Date: " + "<b>" + DateTime.Now.ToString("dd/MM/yyyy") + "</b>" + "<br />" +
              "Time: " + "<b>" + DateTime.Now.ToString("HH:mm") + "</b>" + "<p></p>" + sb.ToString();

            TempData["emailData"] = body;
            emailTo = "ivan.ivanov@mystery-shoppers.co.uk";
            MailMessage msg = md.CreateMailMessage(emailTo, null, body, new System.Web.UI.Control());

            SmtpClient client = new SmtpClient("mail.mystery-shoppers.co.uk", 25);
            NetworkCredential netCredentials = new NetworkCredential("msl.intranet@mystery-shoppers.co.uk", "T4uJVg3R");
            client.UseDefaultCredentials = false;
            client.Credentials = netCredentials;

            client.Send(msg);

            TempData["success"] = true;


            return Content("success");

        }
        public string checkSelected(RequestQuoteModel model)
        {
            StringBuilder sbCheckLstValues = new StringBuilder();

            //foreach (var item in model.lstCheck.Where(x => x.selected == true))
            //{
            //    sbCheckLstValues.Append(item.name + " ");
            //    //if (item.selected)
            //    //{
            //    //    sbCheckLstValues.Append(item.name + " ");
            //    //}
            //}

            return sbCheckLstValues.ToString();
        }
    }
    public class DataHolder
    {
        public string success { get; set; }
    }
}
