﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using System.Xml.XPath;
using Umbraco_Redesign.Models;
using umbraco.presentation.nodeFactory;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using System.Text;
using System.Web.UI.WebControls;


namespace Umbraco_Redesign.Controllers
{
    public class ContactFormController : SurfaceController
    {
        public ActionResult GetFeedbackForm(string sectionId, string enquiryType = "")
        {
            if (Session["hasrequestedinfo"] != null)
            {
                if (Convert.ToBoolean(Session["hasrequestedinfo"]))
                {
                    return PartialView("success");
                }
            }

            ContactFormModel model = new ContactFormModel();

            //model.Name = "Richard Wimbridge";
            //model.Email = "rich@test.com";
            //model.Company = "test Industries";
            //model.Message = "Test Do Not Action!!!";

            //TempData["redirect"] = "no";
            TempData["enquiryType"] = "requestQuote";

            return PartialView("ContactForm", model);
        }



        // GET: ContactFormSurface
        [Route("/ContactForm/?enquiryType={enquiryType}?&type={tabType}?")]
        public ActionResult Index(string sectionId, string enquiryType = "", string type = "")
        {

            ContactFormModel model = new ContactFormModel();

            TempData["enquiryType"] = "contactUs";
            IPublishedContent RequestQuoteNode = Umbraco.TypedContentAtRoot().FirstOrDefault().Descendants().Where(x => x.Id == Convert.ToInt32(sectionId) && x.IsVisible()).FirstOrDefault();
            //Test Data
            //model.Name = "Richard Wimbridge";
            //model.Email = "rich@test.com";
            //model.Company = "test Industries";
            //model.Message = "Test Do Not Action!!!";
            //model.PageId = CurrentPage.Id;
            //TempData["pageId"] = CurrentPage.Id;


            List<SelectListItem> items = new List<SelectListItem>();
            List<string> allowedEnquiryTypes = new List<string>();

            IPublishedContent pricingRootNode = Umbraco.TypedContent(type);

            if (RequestQuoteNode != null)
            {
                foreach (var child in RequestQuoteNode.Children())
                {
                    var enquiryTypeValue = child.GetPropertyValue("enquiryTypeLabel");

                    if (enquiryTypeValue != null && enquiryTypeValue.ToString() != String.Empty)
                    {
                        allowedEnquiryTypes.Add(enquiryTypeValue.ToString());
                    }
                }

                if (allowedEnquiryTypes.Any(x => x.ToLower() == enquiryType.ToLower()))
                {
                    items.Add(new SelectListItem { Text = enquiryType, Value = enquiryType, Selected = true });
                }
            }
            



            XPathNodeIterator iterator = umbraco.library.GetPreValues(1606);
            iterator.MoveNext();
            XPathNodeIterator preValues = iterator.Current.SelectChildren("preValue", "");
             
            while (preValues.MoveNext())
            {
                var currentPrevalue = preValues.Current.Value;
                if (currentPrevalue != String.Empty && currentPrevalue != "0")
                {
                    items.Add(new SelectListItem { Text = preValues.Current.Value, Value = preValues.Current.Value });
                }
            }

            //if(enquiryType != string.Empty && type == "pricingTab")
            //{
            //    items.Add(new SelectListItem { Text = enquiryType, Value = enquiryType, Selected=true });
            //}

            ViewData["Type"] = items;

            return PartialView("ContactForm", model);
        }

        [HttpPost]
        public ActionResult HandleFormSubmit(ContactFormModel model)
        {
            if (!ModelState.IsValid)
            {
                return CurrentUmbracoPage();
            }

            string response = Request["g-recaptcha-response"];
            var recaptchaValidations = Recaptcha.ValidateCaptcha(response);
            var siteId = Convert.ToInt32(Session["siteId"]);

            if (!recaptchaValidations)
            {
                TempData["invalid"] = "Captcha Invalid";
                return PartialView("ContactForm", model);
            }

            try
            {
                // Send Email
                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient("mail.mystery-shoppers.co.uk", 25);
                NetworkCredential netCredentials = new NetworkCredential("msl.intranet@mystery-shoppers.co.uk", "T4uJVg3R");
                client.UseDefaultCredentials = false;
                client.Credentials = netCredentials;

                string type = TempData["enquiryType"].ToString() ?? "";
                string allRecipients = String.Empty;
                string emailContacts = String.Empty;
                var rootNode = Umbraco.TypedContent(siteId);
                string enquiryType = TempData["enquiryType"].ToString() ?? "";
                var selectedItem = model.selectedItem;
                int pageId = -1;

                if (selectedItem == null)
                {
                    selectedItem = "Request Report Information";
                }


                IPublishedContent emailNode = null;

                //emailNode = rootNode.Children().Where(c => c.DocumentTypeAlias == enquiryType).FirstOrDefault();

                //if (emailNode != null)
                //{
                //    emailContacts = emailNode.GetPropertyValue<string>("emailTo") ?? "";
                //}
                //else
                //{
                //    emailContacts = "enquiries@mystery-shoppers.co.uk";
                //}

                if (type != "requestQuote")
                {
                    emailNode = Umbraco.TypedContent(Convert.ToInt32(TempData["pageId"]));
                    //pageId = Convert.ToInt32(TempData["pageId"]);
                }
                else
                {
                    emailNode = Umbraco.TypedContentAtRoot().Where(c => c.IsDocumentType("corporate")).FirstOrDefault().Children().Where(c => c.IsDocumentType(type)).FirstOrDefault();
                    //emailNode = Umbraco.TypedContentAtRoot().Where(c => c.DocumentTypeAlias == "requestQuote").FirstOrDefault();
                    //pageId = Umbraco.TypedContentAtRoot().Where(c => c.DocumentTypeAlias == "requestQuote").FirstOrDefault();
                }


                //if (type != "requestQuote")
                //{
                //    emailNode = rootNode.Children().Where(c => c.DocumentTypeAlias == "requestQuote").FirstOrDefault();
                //    emailContacts = emailNode.GetPropertyValue<string>("emailTo") ?? "";

                //    allRecipients = emailContacts;
                //}
                //else
                //{
                //    emailNode = rootNode.Children().Where(c => c.DocumentTypeAlias == "contactUs").FirstOrDefault();
                //    emailContacts = emailNode.GetPropertyValue<string>("emailTo") ?? "";
                //    allRecipients
                //}


                //UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);

                //var pageId = Convert.ToInt32(TempData["pageId"]);

                // Add as BCC all other recipeints
                // var allRecipients = Model.Content.Site().GetProperty("emailTo");
                //allRecipients = emailContacts;
                //var str = allRecipients.Value;


                //Testing 
                //var str = "richard.wimbridge@mystery-shoppers.co.uk";
                if (emailNode != null)
                {
                    allRecipients = emailNode.GetPropertyValue<string>("emailTo");
                }
                else
                {
                    allRecipients = "enquiries@mystery-shoppers.co.uk";
                }

                var str = allRecipients;

                List<string> recipients = new List<string>();

                recipients = str.ToString().Split(',').ToList();
                var emailTo = "";


                //mail.To.Add(emailTo);
                //var enquiryLWR = model.selectedItem.ToLower().Trim();
                var enquiryLWR = "";

                if (enquiryLWR.Contains("general"))
                {
                    emailTo = recipients[0];
                }
                else if (enquiryLWR.Contains("shopper") && recipients.Count > 1)
                {
                    if (recipients[1] != null)
                        emailTo = recipients[1];
                }
                else if (enquiryLWR.Contains("sales") && recipients.Count > 1)
                {
                    if (recipients[2] != null)
                        emailTo = recipients[2];
                }

                else if (enquiryLWR.Contains("careers") && recipients.Count > 1)
                {
                    if (recipients[3] != null)
                        emailTo = recipients[3];
                }
                else
                {
                    emailTo = recipients[0];
                }

                //emailTo = "richard.wimbridge@mystery-shoppers.co.uk";


                //foreach (var recipient in recipients)
                //{
                //    var recipientLwr = recipient.ToLower();

                //    if(enquiryLWR.Contains("general"))
                //    {
                //        emailTo = recipient;
                //    }
                //    if(enquiryLWR.Contains("shopper"))
                //    {
                //        emailTo = recipient.Where(recipientLwr.ToString() == (String)"shopper.support");
                //    }

                //    //if (recipient != emailTo)
                //    //    mail.Bcc.Add(recipient);
                //}

                //mail.To.Add("richard.wimbridge@mystery-shoppers.co.uk");
                mail.To.Add(emailTo);
                //mail.Bcc.Add("richard.wimbridge@mystery-shoppers.co.uk");
                mail.From = new MailAddress(model.Email, model.Name);
                mail.Subject = selectedItem + " Enquiry From MSL Website";
                //mail.Body = model.Message;
                mail.Body = model.Message;
                client.Send(mail);

                // To get the selected Enquiry item
                TempData["selected"] = model.selectedItem;

                if (enquiryType != "requestQuote")
                {
                    TempData["Success"] = true;
                }
                else
                {
                    Session["hasrequestedinfo"] = true;
                    TempData["hasrequestedinfo"] = true;
                }


            }
            catch (Exception ex)
            {
                TempData["sendError"] = ex.ToString();
            }

            //var redirect = TempData["redirect"];

            //if (redirect != null && redirect.ToString() != "no")
            //{
            //    return RedirectToCurrentUmbracoPage();
            //}

            if (TempData["Success"] != null)
            {
                return PartialView("success", model);
            }

            return PartialView("success");

            //var JsonResult = Json(model, JsonRequestBehavior.AllowGet);
            //return JsonResult;

        }
    }
}