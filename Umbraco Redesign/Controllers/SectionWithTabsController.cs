﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Extensions;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco_Redesign.Models;


namespace Umbraco_Redesign.Controllers
{
    public class SectionWithTabsController : SurfaceController
    {
        // GET: SectionWithTabs
        public ActionResult Index( int tabsId )
        {
            var sectionWithTabsModel = GetSectionWithTabs(tabsId);
            return PartialView("sectionWithTabs", sectionWithTabsModel);
        }

        public List<SectionWithTabsModel> GetSectionWithTabs(int tabsId)
        {
            string tabHeader = String.Empty;
            string tabId = String.Empty;
            object tabContent = null;
            List<SectionWithTabsModel> tabsList = new List<SectionWithTabsModel>();

            IPublishedContent tabsNode = Umbraco.TypedContentAtRoot().FirstOrDefault().Descendants().Where(x => x.Id == Convert.ToInt32(tabsId) && x.IsVisible()).FirstOrDefault();

            if (tabsNode != null)
            {


                foreach (IPublishedContent tab in tabsNode.Children().Where(tn =>tn.IsVisible()))
                {
                    var SectionWithTabModel = new SectionWithTabsModel();
                    tabHeader = tab.GetPropertyValue<string>("tabHeader");
                    tabContent = tab.GetPropertyValue("tabContent");
                    var tabLayout = tab.GetPropertyValue<string>("tabContentLayout");
                    var tabAlias = tab.DocumentTypeAlias;
                    tabId = tab.Id.ToString();


                    if ((tabHeader != "" && tabContent != null) || (tabLayout != null || tabLayout != String.Empty))
                    {
                        SectionWithTabModel.node = tab;
                        SectionWithTabModel.tabHeader = tabHeader;
                        SectionWithTabModel.tabContent = tabContent;
                        SectionWithTabModel.tabsId = Convert.ToInt32(tabId);
                        SectionWithTabModel.tabLayout = tabLayout;
                        SectionWithTabModel.tabAlias = tabAlias;
                        tabsList.Add(SectionWithTabModel);

                    }
                }
            }
            return tabsList;
        }
    }
}