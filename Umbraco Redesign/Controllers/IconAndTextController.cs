﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Extensions;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco_Redesign.Models;

namespace Umbraco_Redesign.Controllers
{
    public class IconAndTextController : SurfaceController
    {
        // GET: IconAndTextG
        public ActionResult Index(string CompanyValueId)
        {
            var iconsAndTextModel = GetIconsandText(CompanyValueId); 
            return PartialView("IconAndTextPage", iconsAndTextModel);
        }
        public List<IconAndTextModel> GetIconsandText(string CompanyValueId)
        {
            string CompanyValueTextArea = String.Empty; 
            string CompanyValueTitle = String.Empty;
            string IconPicker = String.Empty;
            List<IconAndTextModel> IconsAndTextList = new List<IconAndTextModel>();
            string IconPickerUrl = String.Empty;
            string NumberOfValues = String.Empty;
            IPublishedContent IconAndTextNode = Umbraco.TypedContentAtRoot().FirstOrDefault().Descendants().Where(x => x.Id == Convert.ToInt32(CompanyValueId) && x.IsVisible()).FirstOrDefault();

            if(IconAndTextNode != null)
            {
                NumberOfValues = IconAndTextNode.Children().Count().ToString();

                foreach (IPublishedContent iconAndText in IconAndTextNode.Children())
                {
                    var IconAndTextModel = new IconAndTextModel();

                    CompanyValueTextArea = iconAndText.GetPropertyValue<string>("companyValuesText");
                    CompanyValueTitle = iconAndText.GetPropertyValue<string>("companyValuesTitle");
                    IconPicker = iconAndText.GetPropertyValue<string>("iconPicker");

                    if (Umbraco.TypedMedia(IconPicker) != null)
                    {
                        IconPickerUrl = Umbraco.TypedMedia(IconPicker).Url;
                    }
                    if (CompanyValueTextArea != "" && CompanyValueTitle != "")
                    {
                        IconAndTextModel.CompanyValueId = CompanyValueId;
                        IconAndTextModel.CompanyValueTextArea = CompanyValueTextArea;
                        IconAndTextModel.CompanyValueTitle = CompanyValueTitle;
                        if (IconPickerUrl != null)
                        {
                            IconAndTextModel.IconPickerUrl = IconPickerUrl;

                        }
                        IconsAndTextList.Add(IconAndTextModel);
                    }
                }

            }
            return IconsAndTextList;
        }
    }
}