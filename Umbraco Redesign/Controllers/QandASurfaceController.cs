﻿using Microsoft.AspNet.SignalR.Json;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Extensions;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco_Redesign.Models;

namespace Umbraco_Redesign.Controllers
{
    public class QandASurfaceController : SurfaceController
    {
        // GET: QandASurface
        public ActionResult Index(string sectionId)
        {
            var questionAndAnswerModel = GetQuestionsandAnswers(sectionId);

            //return PartialView("bubbles", questionAndAnswerModel);
            return PartialView("flip-tiles", questionAndAnswerModel);
        }

        public string GetAnswerForQuestion(int answerId)
        {
            var answerModel = Umbraco.TypedContent(answerId);
            var answerForQuestion = new AnswerModel();
            if (answerModel != null)
            {
                var answer = answerModel.GetPropertyValue<string>("mSLAnswerText");
                IEnumerable<Link> answerOptions = answerModel.GetPropertyValue<IEnumerable<Link>>("mSLAnswerOptions");

                answerForQuestion = new AnswerModel
                {
                    answerTitle = answerModel.GetPropertyValue<string>("mSLQuestionText"),
                    answerContent = answer,
                    answerLinks = answerOptions
                };
            }
            return JsonConvert.SerializeObject(answerForQuestion);
        }

        public List<QandAModel> GetQuestionsandAnswers(string sectionId)
        {
            //var QandAModel = new QandAModel();
            
            var contentLink = new ContentLinkModel();
            string questionText = String.Empty;
            string answerText = String.Empty;
            List<QandAModel> questionsAndAnswersList = new List<QandAModel>();
            string questionUrl = String.Empty;
            string questionImage = String.Empty;
            string questionImageLowRes = String.Empty;
            string buttonColor = String.Empty;
            string questionImageUrl = String.Empty;
            string questionImageUrlLowRes = String.Empty;
            string questionType = String.Empty;
            string numberOfQuestions = String.Empty;
            int questionAndAnswerId;

            // Intialize QandAModel ContentLink List
            //QandAModel.answerOptions = new List<ContentLinkModel>();

            IPublishedContent questionAndAnswerNode = Umbraco.TypedContentAtRoot().FirstOrDefault().Descendants().Where(x =>x.Id == Convert.ToInt32(sectionId) && x.IsVisible()).FirstOrDefault();

            if(questionAndAnswerNode != null)
            {
                numberOfQuestions = questionAndAnswerNode.Children().Count().ToString();

                foreach (IPublishedContent questionAndAnswer in questionAndAnswerNode.Children())
                {
                    var QandAModel = new QandAModel();
                    QandAModel.answerOptions = new List<ContentLinkModel>();

                    questionUrl =questionAndAnswer.Url;
                    questionText = questionAndAnswer.GetPropertyValue<string>("mSLQuestionText");
                    answerText = questionAndAnswer.GetPropertyValue<string>("mSLAnswerText");
                    questionImage = questionAndAnswer.GetPropertyValue<string>("mSLBubbleImage");
                    questionImageLowRes = questionAndAnswer.GetPropertyValue<string>("mSLBubbleImageLowerRes");
                    questionType = questionAndAnswer.GetPropertyValue<string>("mSLBubbleType");
                    buttonColor = questionAndAnswer.GetPropertyValue<string>("mSLButtonColor");
                    questionAndAnswerId = questionAndAnswer.Id;

                    if (Umbraco.TypedMedia(questionImage) != null)
                    {
                        var fullImageUrl = Umbraco.TypedMedia(questionImage);
                        var extension = fullImageUrl.GetPropertyValue<string>("umbracoExtension");

                        questionImageUrl = Umbraco.TypedMedia(questionImage).Url;
                        //questionImageUrl = fullImageUrl.Url.Replace("."+ extension, "");
                        //questionImageUrl = questionImageUrl + "_preview" + "." + extension;

                        questionImageUrlLowRes = Umbraco.TypedMedia(questionImageLowRes).Url;
                    }
                   

                    if (questionText != "" && answerText != "")
                    {
                        QandAModel.questionUrl = questionUrl;
                        QandAModel.questionId = sectionId;
                        QandAModel.questionAndAnswerId = questionAndAnswerId;
                        QandAModel.questionText = questionText;
                        QandAModel.answerText = answerText;
                        QandAModel.questionType = questionType;
                        QandAModel.buttonColor = buttonColor;

                        if (questionImageUrl != null && questionImageUrlLowRes != null)
                        {
                            QandAModel.questionImageUrl = questionImageUrl;
                            QandAModel.questionImageLowResUrl = questionImageUrlLowRes;
                        }
                        IEnumerable<Link> answerOptions = questionAndAnswer.GetPropertyValue<IEnumerable<Link>>("mSLAnswerOptions");


                        if (answerOptions != null)
                        {

                            foreach(var answerOption in answerOptions)
                            {
                                var target = answerOption.Target;
                                var name = answerOption.Name;
                                var url = answerOption.Url;

                                if(name != "" && url != "")
                                {
                                    contentLink = new ContentLinkModel();
                                    contentLink.contentName = name;
                                    contentLink.contentTarget = target;
                                    contentLink.contentUrl = url;

                                    QandAModel.answerOptions.Add(contentLink) ;
                                }
                                
                            }


                            //string answerOptions = answerOptionsProp.DataValue.ToString();
                            //var answerOptionArray = (Newtonsoft.Json.Linq.JArray)Newtonsoft.Json.JsonConvert.DeserializeObject(answerOptions);



                            //for(var answerOptionCount = 0; answerOptionCount < answerOptionsProp.Count();answerOptionCount++)
                            //{
                            //    //var target = answerOptionArray[answerOptionCount]["target"];
                            //    //var name = answerOptionArray[answerOptionCount]["name"];
                            //    //var udi = answerOptionArray[answerOptionCount]["udi"];
                            //    //var url = answerOptionArray[answerOptionCount]["url"];
                            //    //var socialLinkUrl = answerOptionArray[answerOptionCount]["url"];

                            //    //if(udi != null)
                            //    //{

                            //    //    var test = questionAndAnswer.GetPropertyValue("mSLAnswerOptions");
                            //    //}

                            //    //foreach (var option in answerOptionArray[answerOptionCount])
                            //    //{
                            //    //    var target = option["target"];
                            //    //    var name = option["name"];
                            //    //    var udi = option["udi"];
                            //    //    var url = option["url"];
                            //    //    var socialLinkUrl = option["url"];
                            //    //}
                            //    //var target = answerOptionArray[answerOptionCount]["target"];
                            //    //var socialLinkUrl = answerOptionArray[answerOptionCount]["url"];

                            //}

                            questionsAndAnswersList.Add(QandAModel);
                        }

                        
                    }



                }
            }

            


            return questionsAndAnswersList;

        }
    }
}