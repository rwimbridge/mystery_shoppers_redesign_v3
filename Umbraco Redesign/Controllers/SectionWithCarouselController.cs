﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Extensions;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco_Redesign.Models;

namespace Umbraco_Redesign.Controllers
{
    public class SectionWithCarouselController : SurfaceController
    {
        // GET: SectionWithCarousel
        public ActionResult Index( int carouselId )
        {
            
            var sectionWithCarousel = GetSectionWithCarousel(carouselId);
            return PartialView("Carousel", sectionWithCarousel);
        }

        public List<SectionWithCarouselModel> GetSectionWithCarousel(int carouselId)
        {
            object slideLogo = null;
            string slideLogoUrl = String.Empty;
            string slideId = String.Empty;
            object slideContent = null;
            bool FullWidth = true;
            List<SectionWithCarouselModel> carouselList = new List<SectionWithCarouselModel>();

            IPublishedContent carouselNode = Umbraco.TypedContentAtRoot().FirstOrDefault().Descendants().Where(x => x.Id == Convert.ToInt32(carouselId) && x.IsVisible()).FirstOrDefault();

            if (carouselNode != null)
            {
                foreach (IPublishedContent slide in carouselNode.Children())
                {
                    var SectionWithCarouselModel = new SectionWithCarouselModel();
                    slideLogo = slide.GetPropertyValue<string>("mSLLogoImage");
                    slideLogoUrl = Umbraco.TypedMedia(slideLogo).Url;
                    slideId = slide.Id.ToString();
                    slideContent = slide.GetPropertyValue("mSLSlideText");
                    FullWidth = Convert.ToBoolean(slide.GetPropertyValue("mslCarouselFullwidth"));

                    if (slideLogoUrl != null && slideContent != null)
                    {
                        SectionWithCarouselModel.slideLogoUrl = slideLogoUrl;
                        SectionWithCarouselModel.slideContent = slideContent;
                        SectionWithCarouselModel.slideId = slideId;
                        SectionWithCarouselModel.carouselId = carouselNode.Id;
                        //SectionWithCarouselModel.FullWidth = FullWidth;
                        carouselList.Add(SectionWithCarouselModel);
                    }
                }
            }
            return carouselList;
        }
    }
}