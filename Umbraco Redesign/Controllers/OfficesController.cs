﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Extensions;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco_Redesign.Models;

namespace Umbraco_Redesign.Controllers
{
    public class OfficesController : SurfaceController
    {
        // GET: Offices
        public ActionResult Index(int nodeId)
        {
            var offices = GetOffices(nodeId);
            return PartialView("Office", offices);

        }
        public List<OfficesModel> GetOffices(int nodeId)
        {
            List<OfficesModel> OfficesList = new List<OfficesModel>();
            string officeFlag = String.Empty;
            string officeName = String.Empty;
            string officeAddressName = String.Empty;
            string officeAddressLine1 = String.Empty;
            string officeAddressLine2 = String.Empty;
            string officeAddressLine3 = String.Empty;
            string officeAddressLine4 = String.Empty;
            string officeTown = String.Empty;
            string officeCounty = String.Empty;
            string officePostcode = String.Empty;
            string officePhone = String.Empty;
            string extraPhone = String.Empty;
            string officeEmail = String.Empty;
            string extraHeading = String.Empty;
            string extraHeadingContent = String.Empty;
            string mobilePhone = String.Empty;
            string extraEmail = String.Empty;
            string officeWebsite = String.Empty;
            string officeCountryCode = String.Empty;
            string RegisteredAddressLine1 = String.Empty;
            string RegisteredAddressLine2 = String.Empty;
            string RegisteredAddressLine3 = String.Empty;
            string RegisteredTown = String.Empty;
            string RegisteredPostcode = String.Empty;
            string RegisteredPhone = String.Empty;
            string NumberOfValues = String.Empty;
            string orContactUs = String.Empty;
            string orPressOne = String.Empty;
            string findOutHow = String.Empty;

            IPublishedContent OfficesNode = Umbraco.TypedContentAtRoot().FirstOrDefault().Descendants().Where(x => x.Id == Convert.ToInt32(nodeId) && x.IsVisible()).FirstOrDefault();
            if (OfficesNode != null)
            {
                NumberOfValues = OfficesNode.Children().Count().ToString();

                foreach (var office in OfficesNode.Children())
                {
                    var OfficesModel = new OfficesModel();
                    officeFlag = office.GetPropertyValue<string>("officeFlagImage");
                    officeName = office.GetPropertyValue<string>("officeName");
                    officeAddressName = office.GetPropertyValue<string>("officeAddressName");
                    officeAddressLine1 = office.GetPropertyValue<string>("officeaddressLine1");
                    officeAddressLine2 = office.GetPropertyValue<string>("officeAddressLine2");
                    officeAddressLine3 = office.GetPropertyValue<string>("officeAddressLine3");
                    officeAddressLine4 = office.GetPropertyValue<string>("officeAddressLine4");
                    officeTown = office.GetPropertyValue<string>("officeTown");
                    officeCounty = office.GetPropertyValue<string>("officeCountry");
                    officePostcode = office.GetPropertyValue<string>("officePostCode");
                    officePhone = office.GetPropertyValue<string>("officePhone");
                    extraPhone = office.GetPropertyValue<string>("extraPhone");
                    officeEmail = office.GetPropertyValue<string>("officeEmail");
                    officeWebsite = office.GetPropertyValue<string>("officeWebsite");
                    officeCountryCode = office.GetPropertyValue<string>("officeCountryCode");
                    RegisteredAddressLine1 = office.GetPropertyValue<string>("registeredAddressLine1");
                    RegisteredAddressLine2 = office.GetPropertyValue<string>("registeredAddressLine2");
                    RegisteredAddressLine3 = office.GetPropertyValue<string>("registeredAddressLine3");
                    RegisteredTown = office.GetPropertyValue<string>("registeredTown");
                    RegisteredPostcode = office.GetPropertyValue<string>("registeredPostcode");
                    RegisteredPhone = office.GetPropertyValue<string>("registeredPhone");
                    orContactUs = office.GetPropertyValue<string>("orContactUs");
                    orPressOne = office.GetPropertyValue<string>("orPressOne");
                    findOutHow = office.GetPropertyValue<string>("findOutHow");

                    if (officeName != null && officeAddressName != null)
                    {
                        OfficesModel.officeFlag = officeFlag;
                        OfficesModel.officeName = officeName;
                        OfficesModel.officeAddressName  = officeAddressName;
                        OfficesModel.officeAddressLine1 = officeAddressLine1;
                        OfficesModel.officeAddressLine2 = officeAddressLine2;
                        OfficesModel.officeAddressLine3 = officeAddressLine3;
                        OfficesModel.officeAddressLine4 = officeAddressLine4;
                        OfficesModel.officeCounty = officeCounty;
                        OfficesModel.officePostcode = officePostcode;
                        OfficesModel.officePhone = officePhone;
                        OfficesModel.officeEmail = officeEmail;
                        OfficesModel.officeWebsite = officeWebsite;
                        OfficesModel.officeCountryCode = officeCountryCode;
                        OfficesModel.RegisteredAddressLine1 = RegisteredAddressLine1;
                        OfficesModel.RegisteredAddressLine2 = RegisteredAddressLine2;
                        OfficesModel.RegisteredAddressLine3 = RegisteredAddressLine3;
                        OfficesModel.RegisteredTown = RegisteredTown;
                        OfficesModel.RegisteredPostcode = RegisteredPostcode;
                        OfficesModel.RegisteredPhone = RegisteredPhone;
                        OfficesModel.orContactUs = orContactUs;
                        OfficesModel.orPressOne = orPressOne;
                        OfficesModel.findOutHow = findOutHow;

                        OfficesList.Add(OfficesModel);

                    }
                    
                }
            }
            return OfficesList;
        }
        [ChildActionOnly]
        public ActionResult  GetSocialIcons()
        {
            return PartialView("socialOffices");
        }
    }
}