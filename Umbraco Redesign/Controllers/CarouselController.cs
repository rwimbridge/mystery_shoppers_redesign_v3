﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Extensions;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco_Redesign.Models;

namespace Umbraco_Redesign.Controllers
{
    public class CarouselController : SurfaceController
    {
        // GET: Carousel
        public ActionResult Index(int carouselId)
        {

            var sectionWithCarousel = GetSectionWithCarousel(carouselId);
            return PartialView("Carousel", sectionWithCarousel);
        }
        public CarouselModel GetSectionWithCarousel(int carouselId)
        {

            List<SectionWithSlidesModel> carouselList = new List<SectionWithSlidesModel>();
            object TextBySlide = null;
            bool FullWidth = true;
            List<SectionWithSlidesModel> slides = new List<SectionWithSlidesModel>();
            IPublishedContent carouselNode = Umbraco.TypedContentAtRoot().FirstOrDefault().Descendants().Where(x => x.Id == Convert.ToInt32(carouselId) && x.IsVisible()).FirstOrDefault();
            //IPublishedContent sectionIdNode = CurrentPage;
            CarouselModel carouselModel = null;
            if (carouselNode != null)
            {
                var CarouselSpeed = carouselNode.GetPropertyValue<string>("carouselSpeed");
                TextBySlide = carouselNode.GetPropertyValue("layoutSectionForText");
                FullWidth = Convert.ToBoolean(carouselNode.GetPropertyValue("mslCarouselFullwidth"));
                var TextCarousel = carouselNode.GetPropertyValue<string>("fullRowContent");
                carouselModel = new CarouselModel { carouselId = carouselNode.Id, CarouselSpeed = CarouselSpeed, TextCarousel = TextCarousel, FullWidth = FullWidth, slides = slides };
                carouselModel.slides = GetSlidesForCarousel(carouselModel);
                ////foreach (IPublishedContent carousel in carouselNode.AncestorsOrSelf())
                //{
                //    var CarouselSpeed = carousel.GetPropertyValue<string>("carouselSpeed");
                //    TextBySlide = carousel.GetPropertyValue("layoutSectionForText");
                //    FullWidth = Convert.ToBoolean(carousel.GetPropertyValue("mslCarouselFullwidth"));
                //    var TextCarousel = carousel.GetPropertyValue<string>("fullRowContent");

                //    carouselModel = new CarouselModel { carouselId = carousel.Id, CarouselSpeed = CarouselSpeed, TextCarousel = TextCarousel, FullWidth = FullWidth, slides = slides };
                //    carouselModel.slides = GetSlidesForCarousel(carouselModel);
                //}


            }
            return carouselModel;
        }
        private List<SectionWithSlidesModel> GetSlidesForCarousel(CarouselModel carouselModel)
        {
            IPublishedContent carousel = Umbraco.TypedContent(carouselModel.carouselId);
            var slides = carousel.Children();
            var slideUrl = String.Empty;
            var fullWidth = carouselModel.FullWidth;
            var carouselSlides = new List<SectionWithSlidesModel>();
            /*.Select(s => new SectionWithSlidesModel {slideId = s.Id,}).ToList();*/
            foreach (var slide in slides)
            {
                var slideImageId = slide.GetPropertyValue("mSLLogoImage");
                var SlideImage = Umbraco.TypedMedia(slideImageId.ToString());
                var slideContent = String.Empty;
                if (fullWidth == false)
                {
                    slideContent = slide.GetPropertyValue<string>("mSLSlideText");
                }
                if (SlideImage != null)
                {
                    slideUrl = SlideImage.Url;
                }
                var slideToAdd = new SectionWithSlidesModel
                {
                    slideId = slide.Id,
                    slideLogoUrl = slideUrl,
                    slideContent = slideContent
                };
                if (slideToAdd != null)
                {
                    carouselSlides.Add(slideToAdd);
                }

            }


            return carouselSlides;
        }



    }
}