﻿function allowBounce(tabType) {
    var iconToTarget = $(".imgContainer");


    if (tabType !== null) {
        if (tabType.includes("bespoke")) {
            iconToTarget.addClass("allowBounce");
            var modal = $("#Modal");
            var title = $(modal).find(".modal-title");
            $(title).html("Report Examples");

            BounceIcons(iconToTarget);

        }
        else {
            iconToTarget.removeClass("allowBounce");
            iconToTarget.removeClass("attention");
        }
    }
    else {
        iconToTarget.addClass("allowBounce");
        BounceIcons(iconToTarget);
    }
}

function BounceIcons(iconToTarget) {
    var interval = 2500;
    setInterval(function () {

        if (!iconToTarget.hasClass("allowBounce")) {
            return;
        }

        var modal = $("#Modal");

        if ($(modal).css("display") === "none") {
            iconToTarget.addClass("attention");
            iconToTarget.on("webkitAnimationEnd oanimationend msAnimationEnd animationend", function (e) {

                interval = 6000;

                setTimeout(function () {
                    iconToTarget.removeClass("attention");
                }, interval);

            });
        }

    }, interval);

}


$(document).ready(function () {

    allowBounce(null);

    $(".accordion-link").on("tap", function () {
        $(this).css("background", "green");
        var tabType = $(this).attr("href");
        allowBounce(tabType);
    });

    $(".responsive-tabs li a").on("click", function () {
        var tabType = $(this).attr("href");
        allowBounce(tabType);
    });

    var iconHoverText;

    if (window.outerWidth > 1366) {
        iconHoverText = "Hover";
    }
    else {
        iconHoverText = "Click";
        $(".optionIcon").on("click", function (event) {
            DisplayPopUp(event);
        });
    }

    if ($(".iconHoverText").length > 0) {
        $(".iconHoverText").html("<p>" + iconHoverText + " Icon(s) To See Preview" + "</p>");
    }


    $(".optionIcon").on("mouseover", function (event) {

        $(".modal-backdrop").remove();

        $("#Modal").find(".modal-content").addClass("blueModal");
        $("#Modal").find(".close").addClass("whiteClose");

        DisplayPopUp(event);

    });

    $('#ContactModal').on('hide.bs.modal', function () {

        $("#Modal").modal('hide');

    });

    $("#Modal").on("hide.bs.modal", function () {
        $(".modal-backdrop").remove();
    });

    function DisplayIconExampleInstructions() {
        setTimeout(function () {
            $("#Modal").modal('show');
        }, 200);
    }

    function DisplayPopUp(event) {
        var target = event.target;

        var iconId = $(target).attr("data-id");
        var iconSrc = $(target).attr("src");
        var boxNumber = $(target).attr("data-box-number");



        $.ajax({
            type: "Post",
            url: "/umbraco/Surface/BoxLayout/GetIconContent",
            data: '{ "iconName": "' + iconId + '", "boxNumber": "' + boxNumber + '"}',
            dataType: 'json',
            contentType: 'Application/json; charset=utf-8',
            success: function (result) {

                if (result !== null) {
                    var iconImage = "<img src='" + iconSrc + "'>";
                    var iconContent = "<img style='max-width:100%' src='" + result["iconUrl"] + "'>";
                    var iconType = result["IconType"];
                    var iconTitle = result['Title'];
                    var iconUrl = result['IconUrl'];
                    var demoButtonText = result['DemoButtonText'];
                    var contentToAppend = String.Empty;

                    var targetModal = $("#Modal");
                    var modalTitle = targetModal.find(".modal-title");
                    var modalBody = targetModal.find(".modal-body");

                    var isClient = false;

                    if (isClient === false) {
                        $(modalTitle).html(iconImage + " " + iconTitle + "<a href ='javascript:void(0)' style='padding-top: 5px; padding-bottom: 0;' class='btn btn-Request-Demo btn-round'><p style='white-space: normal; padding: 0;'>Click Here " + demoButtonText + "</p></a>");
                    }
                    else {
                        $(modalTitle).html(iconImage + " " + iconTitle);
                    }

                    if (iconType !== "mp4" && iconType !== "mpg" && iconType !== "pdf" && iconType !== "link") {

                        contentToAppend = "<div style='width: 100%; overflow-y: auto; height: 100%'><img style='max-width:100%' src='" + iconUrl + "'></div>";
                    }
                    else if (iconType === "pdf" || iconType === "link") {
                        if (window.outerWidth > 1300) {
                            contentToAppend = "<iframe width='100%' height='100%' style='overflow: hidden' src='" + iconUrl + "' seamless webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> ";
                        }
                        else {

                            // Load the Pdf using pdf.js
                            //var url = result["iconUrl"];

                            contentToAppend = "<iframe width='100%' height='100%' style='overflow: hidden' src='" + iconUrl + "' seamless webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> ";

                        }

                    }
                    else {

                        contentToAppend = "<video width='100%' height='95%' id='my_video_1' class='video-js vjs-default-skin' preload='auto' controls='' loop='' data-setup='{}'> <source src='" + iconUrl + "' type='video/mp4'> </video>"

                    }


                    $(modalBody).html("");

                    $(modalBody).html(contentToAppend);
                    $(targetModal).appendTo("body");
                    //$(".modal-dialog").css({ "position":"fixed","top":"50%","left":"50%","width": "60%"  });
                    //$("#Modal .modal-content").css({ "height": "80vh" });


                    //var pos = GetTargetPosition(event);
                    //posX = pos.x;
                    //posY = pos.y;

                    setTimeout(function () {
                        $("#Modal").modal('show');
                        $(".model").css("display", "block");

                        if ($("#Modal").hasClass("in")) {
                            // do nothing
                        }
                        else {
                            $("#Modal").addClass("in");
                            $("#Modal").css("display", "block");
                        }

                    }, 500);



                    //ShowExample(posX, posY);
                }


            },
            error: function () {
                console.log("error");
            }

        });


        var isClient = getUrlParameter("client");

        if (isClient === null) {
            isClient = "";
        }

        if (isClient === "") {
            DisplayContactForm();
        }

    }

    function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };

    function DisplayContactForm() {

        var enquiryType = "";
        var targetModal = $("#ContactModal");
        var modalHeader = targetModal.find(".modal-header");
        var modalTitle = targetModal.find(".modal-title");
        var modalBody = targetModal.find(".modal-body");
        console.log("display contact form");

        $(modalTitle).html("Request Reporting Demo");

        $.ajax({
            type: "GET",
            url: "umbraco/Surface/ContactFormSurface/GetFeedbackForm",
            data: '{ "enquiryType": "' + enquiryType + '"}',
            success: function (result) {

                console.log("feedback form result");
                $("#ContactModal").find(".modal-body").html(result);

                setTimeout(function () {
                    $("#ContactModal").modal('show');
                }, 4000);


            },
            error: function (error) {
                console.log("error");
            }

        });
    }

    function GetTargetPosition(event) {
        var posX = (event.pageX - $(".responsive-tabs-container").offset().left) - 200;
        var posY = (event.pageY - $(".responsive-tabs-container").offset().top) - 750;

        return { x: posX, y: posY };
    }

    function ShowExample(x, y) {
        var target = $("#optionExample");


        target.css("top", y);
        target.css("left", x);

        if (target.css("display") === "none") {
            target.show();
        }


    }


});