
/*navbar dropdowns hover effect */


$(".nav-tab-link").click(function (event) {
    //event.preventDefault();
    //event.stopPropagation();
    
});

$('.dropdown').hover(
    function(){
      $('.overlay').addClass('overlay-show');
    },
    function(){
      $('.overlay').removeClass('overlay-show');
    });

$(".get-started").on("mouseenter", function () {
    $(this).addClass("get-started-hover");
});

$(".get-started").on("mouseleave", function () {
    $(this).removeClass("get-started-hover");
});

/* go to another container when scroll */

var scrollCount = 1; //this counter counts how many times user scrolls
var timeout;         // this prevents scrolling twice on the About Us page

/* side menu navbar */

function openNav() {
    $(".sidenav").addClass('sidenav-click');
  }
  function closeNav() {
    $(".sidenav").removeClass('sidenav-click');
}

function showFirstActiveTab() {
    var sections = $(".section");
    $.each(sections, function (index, value) {
        var tabs = $(sections[index]).find(".section-with-tabs");
        var tabLink = $(tabs).find(".nav-tab-link");
        var tabPane = $(tabs).find(".tab-pane");
            $(tabLink[0]).addClass("active");
            $(tabPane[0]).addClass("active show");
    });
}


$(document).ready(function () {
    var locationHash = location.hash;


    showFirstActiveTab();
    

    //if (locationHash !== "") {
    //    var elementExists = $(locationHash);
    //    if (elementExists !== null) {
    //        console.log("animate");
    //        //$('.test').animate({
    //        //    scrollTop: $(locationHash).offset().top
    //        //}, 700);
    //        var scrollOffset = -120;

    //        $(".test").animate({
    //            scrollTop: $(locationHash).offset().top - $(".test").offset().top + $(".test").scrollTop() + scrollOffset
    //        }, 700);
    //    }

        
    //}

    var url = window.location.href;
    var urlPath = window.location.pathname.toString();
    
    if (urlPath.endsWith("/")) {
        urlPath = urlPath.substring(urlPath.length - 1, 1);
    }
    var lastSlash = urlPath.lastIndexOf("/");
    if (lastSlash !== null) {
        urlPath = urlPath.substring(lastSlash + 1, urlPath.length);
    }
  
    var replacedUrl = urlPath.replace(/-/g, " ");
    var match = replacedUrl.match(/[/]([\w]||[\s])+[/]$/);

    var articleTitle = replacedUrl;
    var currentUrl = "";

    if (match) {
        articleTitle = replacedUrl.match(/[/]([\w]||[\s])+[/]$/)[0].replace(/[/]/g, "").toLowerCase();
    }

    articleTitle = articleTitle.replace("/", "");

    //var articleTitle = "1393";

    $.ajax({
        type: "POST",
        //url: '@Url.Action("GetIdFromTitle", "SolutionsSurface")',

        url: "/umbraco/Surface/SectionsSurface/GetIdFromTitle",
        data: '{ "title": "' + articleTitle + '"}',
        dataType: 'json',
        contentType: 'Application/json; charset=utf-8',
        success: function (data) {
            if (data.toString() === "-1") {
                return;
            } else {
                var modal = $("#Modal");
                $(modal).css("display", "block");
                $(modal).find(".modal-title").html(data.modalHeader);
                $(modal).find(".modal-body").html(data.modalContent);
                var articleTitle = data.modalHeader;
                if (articleTitle !== null) {
                    articleTitle = articleTitle.toString().toLowerCase().replace(/[\s]/g, "-").replace(/[.]/g, "");
                    //window.history.pushState(null, null, articleTitle);
                }
                $(modal).find(".modal-body").html("<div class='modal-image'></div>" + data.modalContent);
                var modalImage = $(modal).find(".modal-image");

                if (data.modalImageUrl !== null) {
                    $(modalImage).css('background-image', "url('" + data.modalImageUrl + "')");
                }
                else {
                    $(modalImage).remove();
                }
                
                $(modal).modal("show");
                //DisplayArticle(data, url);
            }
        }
    });

    GetSectionId();

    AddWayPoints();

    $(".clickable").click(function (event) {
        if ($(this).hasClass("hasModal")) {
            event.preventDefault();
        }
        
    });

    $(".nav-link").on("click", function (event) {

        if ($(event.target).hasClass("tab-link") || $(event.target).hasClass("nav-tab-link")) {
            var element = $(event.target);
            var elementId = $(element).attr("id");
            var closestSection = $(element).closest(".section-with-tabs");
            //var tabs = $(closestSection).find(".nav-tab-link");
            var panes = $(closestSection).find(".tab-pane");
            $(panes).removeClass("active show");


            //if (tabs.length > 0) {
            //    $.each(tabs, function (index, value) {
            //        $(value).removeClass("active");
            //    });
            //}

            //if (panes.length > 0) {
            //    $.each(panes, function (index, value) {
            //        $(value).removeClass("active show");
            //    });
            //}

            $("#nav-" + elementId).addClass("active show");
            var clickElement = $(event.target).attr("id");
            $("#section" + sectionId).find(".tab-pane").removeClass("active show");
            $("#nav-" + clickElement).addClass("active show");




            //$(".tab-pane").removeClass("active show");
            //$("#nav-video-shopping").addClass("active show");

            return;
            //event.preventDefault();
        }

        if ($(".test").scrollTop() > 0) {
            $(".test").scrollTop(0, 0);
        }
    });

    /* Search Button */
    $("#button-addon").on("click", function () {
        $('#search-dropdown').toggleClass("search-results-show");
    });

    /* Animate the page scrolling */
    $(document).on('click', 'a[href^="#cont"]', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 700);
    });

    //var locationHash = window.location.hash;

    $(".navbar").removeClass("fixed-top");

    var contentWrappers = $(".contentWrapper");
    
    $(document).scroll(function () {

        var hasOverlay = $(".overlay").hasClass("overlay-show");

        if (hasOverlay) {
            return;
        }
    });

    $(".navbar .nav-item").mouseenter(function () {
        $(this).find(".dropdown-menu").show();
        $(".overlay").addClass("overlay-show");
    });

    $(".navbar .dropdown-item").click(function () {
        $(this).closest(".dropdown-menu").fadeOut();
        $(".overlay").removeClass("overlay-show");
    });

    $(".navbar .nav-item").mouseleave(function () {
        console.log("left nav item");
        $(this).find(".dropdown-menu").fadeOut();
        $(".overlay").removeClass("overlay-show");
    });

    $(document).on('click', function (event) {
        var target = $(event.target);

        if ($(target).hasClass("tab-nav-item") || $(target).hasClass("nav-item") || $(target).hasClass("nav-link") || $(target).hasClass("dropdown-item") || $(target).hasClass("search-link")) {
            event.preventDefault();

            //if($(target).attr("id") == )

            //var uri = window.location.toString();
            
        }

        if ($(event.target).hasClass("tab-link")) {
            return;
        }

        GetPage(event);

        //var target = event.target;
    });

});

// 

//function openNav() {
//    $(".sidenav").css('-moz-transform', 'translate(0px)');
//    $(".sidenav").css('-webkit-transform', 'translate(0px)');
//}
//function closeNav() {
//    $(".sidenav").css('-moz-transform', 'translate(300px)');
//    $(".sidenav").css('-webkit-transform', 'translate(300px)');
//}

function SetFade(element, type) {
    var removeClass;

    if (type === "fadeOut") {
        removeClass = "fadeIn";

    }
    else if (type === "fadeIn") {
        removeClass = "fadeOut";
    }

    $(element).removeClass(removeClass);
    $(element).addClass(type);
}

// Page Retrieval Functions

function GetSectionId() {
    var href = window.location.pathname;

    href = href.replace(/^\/|\/$/g, '').replace(/-/g, ' ').split('/');
    var pageTitle = href[href.length - 1];

    $.ajax({
        type: "POST",
        //url: '@Url.Action("GetSectionId", "SectionsSurface")',
        url: "/umbraco/Surface/SectionsSurface/GetSectionId",
        contentType: 'Application/json; charset=utf-8',
            data: '{"pageTitle": "' + pageTitle + '"}',
                success: function (data) {
                    if (data !== "-1") {
                        var linkHash = data;
                        var sectionId = data.replace("#section", "");
                        
                        // Get The Page
                        GetSinglePage(sectionId, linkHash);
                    }

                    // If the hash exists scroll to it
                    var contentToScroll = $(".test");
                    var scrollOffset = -120;
                    var locationHash = location.hash;
                    if (locationHash !== "") {
                        var navTab = locationHash + "-tab";
                        var elementExists = $(locationHash);
                        if (elementExists !== null) {
                            $(contentToScroll).animate({
                                scrollTop: $(navTab).offset().top - $(contentToScroll).offset().top + $(contentToScroll).scrollTop() + scrollOffset
                            }, 700);

                            var section = $(navTab).closest(".section");
                            var navTabLink = $(section).find(".nav-tab-link");
                            var navPanes = $(section).find(".tab-pane");
                            var navPane = $(section).find(locationHash);
                            
                           
                            $(navTabLink).removeClass("active");
                            $(navPanes).removeClass("active");
                            $(navTab).addClass("active");
                            $(navPane).addClass("active").addClass("show");
                            //$(section).find(navTab).addClass("active");
                            //$(navPane).addClass("active").addClass("show");

                        }


                    }
                },
                error: function (err) {
                    alert("error getting section id");
                }

    });
}

function GetIndex() {
    //$.ajax({
    //    type: "POST",
    //    //Url: '@Url.Action("Index","SectionsSurface")',
    //    url: "/umbraco/Surface/SectionsSurface/Index",
    //    contentType: 'Application/json; charset=utf-8',
    //    success: function (data) {
    //        $("#sectionContent").html(data);
    //        var href = window.location.pathname;
    //        href = href.replace(/^\/|\/$/g, '').replace(/-/g, ' ').split('/');
    //        var pageTitle = href[href.length - 1];
    //    },
    //    error: function (err) {
    //        alert("error occured");
    //    }
    //});
}

function GetPage(event) {

    

    var target = event.target;
    var addressUrlPath = $(target).attr("data-href");

    if (addressUrlPath != null) {
        addressUrlPath = $(target).attr("data-href").toString();

        addressUrlPath = addressUrlPath.replace("/", "");

        if (window.location.href !== addressUrlPath) {
            //history.replaceState(null, null, addressUrlPath);
        }

    }

    var sectionId = "0";
    var linkHash = "#section" + $(target).attr("data-id");
    var hasLinkHash = false;

    if (linkHash !== null && linkHash !== "") {
        sectionId = $(target).attr("data-id");
        hasLinkHash = true;
    }
    else {
        sectionId = $(target).attr("data-id");
    }

    if ($(target).hasClass("nav-item") || $(target).hasClass("dropdown-item") || $(target).hasClass("nav-link") || $(target).hasClass("search-link")) {
        
        if (!$(target).hasClass("nav-tab-link")) {
            GetSinglePage(sectionId, linkHash, addressUrlPath);
        }

    }
}

function GetSinglePage(sectionId, linkHash, href) {

    $.ajax({
        type: "POST",
        //url: '@Url.Action("GetSinglePage","SectionsSurface")',
        url: "/umbraco/Surface/SectionsSurface/GetSinglePage",
        contentType: 'Application/json; charset=utf-8',
        data: '{"sectionId": "' + sectionId + '"}',
        dataType: "html",
        success: function (data) {
            if (data !== "") {
                $("#sectionContent").html(data);
                //alert("has data");


                $(".nav-tab-link").click(function (event) {
                    var clickElement = $(event.target).attr("id");
                    var sectionWithTabs = $(event.target).closest(".section-with-tabs");

                    $(sectionWithTabs).find(".tab-pane").removeClass("active show");
                    $("#nav-" + clickElement).addClass("active show");
                });

                $(".clickable").click(function (event) {
                    event.preventDefault();
                });

                AddWayPoints();

                if (linkHash !== "") {
                    var contentToScroll = $(".test");
                    var scrollOffset = -120;
                    var first = $(".sectionContentWrapper")[0];
                    var firstSectionContentWrapper = $(".section")[0];
                    var firstSection = $(firstSectionContentWrapper).attr("id");
                    var firstSectionId;
                    if (firstSection !== null) {
                        firstSectionId = firstSection.replace("section", "");

                        if (firstSectionId === sectionId) {
                            $(first).removeClass("text-focus-in");
                        }
                    }

                    if ($(linkHash).offset() !== null && $(contentToScroll).offset() !== null) {
                        if ($(linkHash).offset() !== null && $(contentToScroll).offset() !== null) {
                            if ($(linkHash).offset() != null) {
                                $(contentToScroll).animate({
                                    scrollTop: $(linkHash).offset().top - $(contentToScroll).offset().top + $(contentToScroll).scrollTop() + scrollOffset
                                }, 700);
                            }
                            
                        }

                    }

                    var parent = $(linkHash).parent();
                    //$(linkHash).find(".");

                    SetFade($(first), "fadeIn");

                    //showFirstActiveTab();


                }

                // Set the active tab
                //if (string.includes(href,0)) {
                //    console.log("contains hash");
                //}
                //showFirstActiveTab();
                var activeTabs = $("#" + sectionId).find(".nav-tab-link").find("active");

                //var hasActive = $(activeTabs).find("active");


                if (activeTabs.length === 0) {
                    showFirstActiveTab();
                }

                if (href === null || href === "") {
                    var tabsInSection = $("#section" + sectionId).find(".nav-tab-link");
                    var tabPanesInSection = $("#section" + sectionId).find(".tab-pane");
                    if (tabsInSection.length > 0 && tabPanesInSection.length > 0) {




                        //$(tabsInSection[0]).addClass("active");
                        //$(tabPanesInSection[0]).addClass("active show");

                    }

                    return;
                }
                else {
                    if (href != null && href.includes("/")) {
                        return;
                    }
                }
                

                var tabPane = "#nav-" + href;
                var tabLink = "#" + href;
                //showFirstActiveTab();


                //$(tabLink).addClass("active");
                //$(tabPane).addClass("active show");
                if ($(tabPane) !== null && $(tabLink) !== null) {

                    $("#section" + sectionId).find(".nav-tab-link").removeClass("active");
                    $("#section" + sectionId).find(".tab-pane").removeClass("active show");
                    $(tabLink).addClass("active");
                    $(tabPane).addClass("active show");

                    

                    var activeElements = $("#section" + sectionId).find(".nav-tab-link.active");
                    if (activeElements.length === 0) {
                        
                        showFirstActiveTab();
                    }

                }

                //$("#nav-mystery-shopping-vacancies-tab").addClass("active");
                //$("#nav-mystery-shopping-vacancies").addClass("active show");





            }
            else {
                alert("no data");
            }
        },
        error: function (err) {
            alert("error occured");
        }
    });

}

$("#Modal").on("hidden.bs.modal", function () {
    var modal = $("#Modal");
    var modalFooter = $("#Modal").find(".modal-footer");
    var modalContent = $("#Modal").find(".modal-content");
    var modalClose = $("#Modal").find(".close");
    $(modalFooter).html("");
    $(modalContent).removeClass("whiteModal").removeClass("blueModal");
    $(modalClose).removeClass("whiteClose").removeClass("darkClose");
});


