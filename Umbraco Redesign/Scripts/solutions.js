$(function () {
    $('.modalPopUp').click(function () {
        
        var solutionImage = $(this).find(".solution-image").css('background-image');
        var solutionTitle = $(this).find('h5').html();
        var solutionContent = $(this).parent().find('.modal-text').html();
        
        
        var modal = $("#Modal");
        $(modal).css("display", "block");
        var modalBody = $(modal).find(".modal-body");
        var modalImage;
        var modalTitle = $(modal).find(".modal-title");
        var modalContent = $(modal).find(".modal-content");
        var modalClose = $(modal).find(".close");

        $(modalBody).html("<div class='modal-image'></div>" + solutionContent);
        modalImage = $(modal).find(".modal-image");
        $(modalImage).css('background-image', solutionImage);
        $(modalTitle).html(solutionTitle);
        $(modalContent).addClass("whiteModal");
        $(modalClose).addClass("darkClose");
        var articleTitle = modalTitle.text();
        if (articleTitle !== null) {
            articleTitle = articleTitle.toString().toLowerCase().replace(/[\s]/g, "-").replace(/[.]/g, "");
            window.history.pushState(null, null, location.pathname + articleTitle);
        }
        $(modal).modal("show");
    });
});