$(document).ready(function () {

    $(".flip-card").on("click", function (event) {
        var eventTarget = $(event.target);
        event.stopImmediatePropagation();
        event.preventDefault();
        var flipCard = $(event.target).closest(".flip-card");

        if ($(eventTarget).hasClass("clickable") || $(eventTarget).parent().hasClass("clickable")) {

            if ($(eventTarget).hasClass("hasModal")) {
                var flipCardId = $(flipCard).attr("flip-card-id");
                GetReadMoreContent(flipCardId);
            }

            return;
        }
        FlipCard($(flipCard));


        

        

        

    });






    $(".thecard").each(function () {
        var cardBackHeight = $(this).find(".back").height();
        var cardBackTextHeight = $(this).find("p").height();
        var backImageHeight = 250 - cardBackTextHeight;
        var cardHeight = $(this).height();

        if (cardBackHeight > 300) {
            $(this).parent().addClass("grid-column-span");
            $(this).css("height", 300);
            $(this).find('h6').css("width", 420);
            cardBackTextHeight = $(this).find("p").height();
            backImageHeight = 250 - cardBackTextHeight;
            $(this).find(".small-back-image").css("height", backImageHeight);
        }
        if (backImageHeight > 10) {
            console.log('back image more then zero');
            $(this).find(".small-back-image").css("height", backImageHeight);
        };


    });
    $(".thecard").on("click", function () {
        $(".thecard").removeClass('selected-card');
        $(this).addClass('selected-card');

        var buttonHasBeenClicked = $(this).find(".btn-video");
        if (buttonHasBeenClicked.length === 0) {

            $(this).find(".buttons").on("click", function (event) {
                event.stopPropagation()
            });

            $(".thecard").each(function () {
                if ($(this).hasClass("selected-card")) {
                    $(this).toggleClass("flip-the-card")
                } else {
                    $(this).removeClass('flip-the-card');
                }
            });


        }

    });
});

function FlipCard(cardToFlip) {
    console.log("flip card");
    var cardOverlay = $(cardToFlip).find(".blue-overlayQandA");
    var flipCardTitle = $(cardToFlip).find(".flip-card-title");
    var selectedCard = $(cardToFlip).closest(".flip-card");

   

    if ($(cardToFlip).hasClass("flip-front-card")) {
        animateCard($(cardToFlip));
        $(cardToFlip).removeClass("flip-front-card selected");
        
        //$(cardToFlip).find(".flip-card-front").toggle();
        //$(cardToFlip).find(".flip-card-back").toggle();
        //$(cardToFlip).removeClass("flip-front-card");
        //$(cardToFlip).find(".card-front-image-container").show();
        //$(flipCardTitle).show();
        //$(cardToFlip).find(".card-title").toggle();
        //$(cardOverlay).toggle();

    }
    else {

        
        animateCard(cardToFlip);
        $(cardToFlip).addClass("flip-front-card selected");
        //$(cardToFlip).find(".flip-card-front").toggle();
        //$(cardToFlip).find(".flip-card-back").toggle();
        
        CloseOpenCards($(cardToFlip));

        
        

        //for (var cardCount = 0; cardCount < cardsOpen.length; cardCount++) {
        //    //var card = $(cardsOpen[cardCount]);
        //    ////if (!$(card).hasClass("selected")) {
        //    //$(card).find(".card-front-image-container").show();
        //    //$(card).find(".card-title").toggle();
        //    //$(card).removeClass("flip-front-card");
        //    //$(card).find(".flip-card-title").show();
        //    //$(card).find(".blue-overlayQandA").toggle();

        //    //}
        //}

        //var selectedCard = $(cardToFlip).closest(".flip-card");
        //var selectedCardFrontImage = $(selectedCard).find(".card-front-image-container");
        //var cardTitle = $(cardToFlip).find(".card-title");

        //$(selectedCardFrontImage).hide();
        //$(cardOverlay).toggle();
        //$(flipCardTitle).hide();
        //$(cardTitle).toggle();
        //$(selectedCard).addClass("flip-front-card selected");
        //$(".flip-card").removeClass("selected");
        //$(selectedCard).addClass("selected");

        
    }
    
}



function CloseOpenCards(cardToFlip) {
    var cardsOpen = $(".flip-front-card");
    console.log(cardsOpen.length);
    for (var cardCount = 0; cardCount < cardsOpen.length; cardCount++) {
        var card = $(cardsOpen[cardCount]);
        console.log($(card));

        console.log(card);
        console.log(cardToFlip);
        if ($(card).attr("flip-card-id") !== $(cardToFlip).attr("flip-card-id")) {
            console.log("card is not equal");
            animateCard($(card));
            $(card).removeClass("flip-front-card selected");
            
            //$(card).find(".flip-card-front").toggle();
            //$(card).find(".flip-card-back").toggle();
        }

        //if (!$(card).hasClass("selected")) {
            
        //}
        //else {
            
        //}
        
        
        //var card = $(cardsOpen[cardCount]);
        ////if (!$(card).hasClass("selected")) {
        //$(card).find(".card-front-image-container").show();
        //$(card).find(".card-title").toggle();
        //$(card).removeClass("flip-front-card");
        //$(card).find(".flip-card-title").show();
        //$(card).find(".blue-overlayQandA").toggle();

        //}
    }
}

function GetReadMoreContent(flipCardId) {
    $.ajax({
        method: "post",
        url: "/umbraco/Surface/QandASurface/GetAnswerForQuestion",
        contentType: "Application/json; charset=utf-8",
        dataType: "json",
        data: "{ 'answerId': " + flipCardId + "}",
        success: function (response) {
            console.log("flip card result");
            if (response !== null) {

                ShowReadMoreContent(response);
            }

        },
        error: function (error) {

        }
    });
}

function ShowReadMoreContent(contentToShow) {


    var modal = $("#Modal");
    var modalTitle = $(modal).find(".modal-title");
    var modalContent = $(modal).find(".modal-content");
    var modalBody = $(modal).find(".modal-body");
    var flipCardLinks = "";
    var contentLinks = contentToShow.answerLinks;

    flipCardLinks += "<ul class='flip-card-links'>";

    for (var i = 0; i < contentToShow.answerLinks.length; i++) {
        flipCardLinks += "<li><a class='card-link clickable' target='" + contentLinks[i].Target + "' href='" + contentLinks[i].Url + "'>" + contentLinks[i].Name + "</a></li>";
    }

    flipCardLinks += "</ul>";

    $(modalContent).addClass("whiteModal");

    $(modalTitle).text(contentToShow.answerTitle);
    $(modalBody).html("<div>" + contentToShow.answerContent + "</div>" + "<div>" + flipCardLinks + "</div>");
    console.log(contentToShow.answerLinks);
    $(".modal-image").remove();
    $(modal).modal("show");

}

function animateCard(cardToFlip) {
    console.log("animate card");
    if ($(cardToFlip).hasClass("flip-front-card")) {
        console.log("has flip card front");
        $(cardToFlip).find(".flip-card-front").show();
        $(cardToFlip).find(".flip-card-back").hide();
    }
    else {
        console.log("doesnt");
        $(cardToFlip).find(".flip-card-front").hide();
        $(cardToFlip).find(".flip-card-back").show();
    }
    //$(cardToFlip).find(".flip-card-front").toggle();
    //$(cardToFlip).find(".flip-card-back").toggle();
}

