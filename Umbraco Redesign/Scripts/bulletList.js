﻿$(document).ready(function () {
    $(".findOutMore").click(function (event) {

        var nodeId = $(event.target).attr("data-item");

        event.preventDefault();

        $.ajax({
            method: "post",
            url: "/umbraco/Surface/BulletList/GetBulletListItemContent",
            contentType: "Application/json charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ nodeId: nodeId }),
            success: function (data) {
                console.log(data);
                DisplayContent(data);
            },
            error: function (error) {

            }
        });


        function DisplayContent(bulletListItem) {
            var modal = $("#Modal");

            $(modal).find(".modal-title").text(bulletListItem.Title);
            $(modal).find(".modal-body").html(bulletListItem.Content);
            $(modal).find(".modal-content").css("backgroundColor", "#fff");
            $(modal).find(".modal-content").css("color", "#09345E");
            $(modal).find(".modal-content").addClass("modalWhite bulletList");
            $(modal).find(".close").addClass("closeDark");
            $(modal).modal("show");
        }


    });
});