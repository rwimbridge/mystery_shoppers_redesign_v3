//$(function () {
    $(".thecard").each(function () {
        var cardBackHeight = $(this).find(".back").height();
        var cardBackTextHeight = $(this).find("p").height();
        var backImageHeight = 250 - cardBackTextHeight;
        var cardHeight = $(this).height();
        
        if (cardBackHeight > 300) {
            $(this).parent().addClass("grid-column-span");
            $(this).css("height", 300);
            $(this).find('h6').css("width", 420);
            cardBackTextHeight = $(this).find("p").height();
            backImageHeight = 250 - cardBackTextHeight;
            $(this).find(".small-back-image").css("height", backImageHeight);
        }
        if (backImageHeight > 10) {
            console.log('back image more then zero');
            $(this).find(".small-back-image").css("height", backImageHeight);
        };
       
        
    });
$(".thecard").on("click", function () {
        $(".thecard").removeClass('selected-card');
        $(this).addClass('selected-card');

        var buttonHasBeenClicked = $(this).find(".btn-video");
        if (buttonHasBeenClicked.length === 0) {

            $(this).find(".buttons").on("click", function (event) {
                event.stopPropagation()
            });

            $(".thecard").each(function () {
                if ($(this).hasClass("selected-card")) {
                    $(this).toggleClass("flip-the-card")
                } else {
                    $(this).removeClass('flip-the-card');
                }
            })


        }

    });
//})