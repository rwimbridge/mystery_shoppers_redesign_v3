﻿$(function () {
    $('.choose-card-btn').on('click', function () {
        $('.dropdown-questions').toggleClass('d-none')
    });

    $('.mobile-card').on('click', function () {
        var dataId = '#' + $(this).data('id');
        $(dataId).toggleClass('d-none');
        
    })
})