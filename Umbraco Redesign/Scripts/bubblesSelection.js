$(function () {


    //show the first question and answer inside of the large central bubble by default

    selectBubble($('#b0'));
    setTextContainerPosition();

    //show the question&answer of the selected bubble inside of the large central bubble when it is clicked


    $(".bubble").on("click", function () {
        $('.centerBubble').find('.textContainer').css('opacity', '0');
        $(".bubble").removeClass("selected");
        $(this).addClass('selected');
        $('.centerBubble').addClass('flip-vertical-right');
        $('.centerBubble').find('.textContainer').empty();
        setTimeout(function () {
            $('.centerBubble').removeClass('flip-vertical-right');
        }, 1000);
        selectBubble($(this));

        

    })

    //the translateY setting

    function setTextContainerPosition() {
        //console.log(bubblesArray);
        $(".bubble").each(function () {
            var bubbleHeight = function (element) {
                if (element.hasClass("selected")) {
                    return element.height() + 6
                } else {
                    return element.height()
                }
            }
            var textContainerHeight = $(this).find("h2").height();
            var translateY = "translateY(-" + ((bubbleHeight($(this)) - textContainerHeight) / 2 + textContainerHeight) + "px)";
            $(this).find('.textContainer').css("transform", translateY);
            
        })


    }




    //the function which changes the text inside of the large bubble
    

    function selectBubble(bubble) {
        var questionText = bubble.find('h2');
        var answer = bubble.find('p');
        var links = bubble.find('ul');
        var centerBubble = $('.centerBubble').find('.textContainer');
        //var backgroundImage = $(this).find('.bubble-image').css('background-image');
        //backgroundImage = backgroundImage.replace('url(', '').replace(')', '').replace(/\"/gi, "");
        //var backgroundImageUrl = 'url(@"' + backgroundImage + '")';
        //console.log(backgroundImageUrl);
        //$('.centerBubble').find('.bubble-image').css('background-image', 'iii' );
        ////$('.centerBubble').find('.bubble-image').css('background-color', "#000" );

        setTimeout(function () {
            $('.centerBubble').find('.textContainer').css('opacity', '1');
            questionText.clone().appendTo(centerBubble);
            answer.clone().appendTo(centerBubble);
            links.clone().appendTo(centerBubble);
        }, 1000)

        setTimeout(function () {
            var centerBubble = $('.centerBubble').height();
            var textContainerHeight = $('.centerBubble').find('.textContainer').height();
            
            var paddingTop = (centerBubble - textContainerHeight) / 2 + 6;
            $('.centerBubble').find('.textContainer').css("padding-top", paddingTop);
        }, 1010)


    }
});